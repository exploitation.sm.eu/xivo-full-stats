package xivo.fullstats.integration

import org.joda.time.Period
import xivo.fullstats.CallEventsLoop
import xivo.fullstats.model.{QueueLog, StatQueuePeriodic, StatQueuePeriodicTestUtils}
import xivo.fullstats.queue.StatQueueManager
import xivo.fullstats.testutils.DBTest

class QueuePeriodicIntegrationSpec extends DBTest(List("queue_log", "stat_queue_periodic")) {

  "StatQueueManager" should "insert the new StatQueuePeriodic" in {
    val existingPeriod = StatQueuePeriodic(None, format.parseDateTime("2014-01-01 08:00:00"), "queue1",
      1, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0)
    StatQueuePeriodic.insert(existingPeriod)
    val queueLogs = List(
      QueueLog(1, format.parseDateTime("2014-01-01 08:11:00"), "123456.789", "queue1", "NONE", "ENTERQUEUE", None, None, None, None, None),
      QueueLog(2, format.parseDateTime("2014-01-01 08:12:00"), "123456.789", "queue2", "NONE", "ENTERQUEUE", None, None, None, None, None),
      QueueLog(3, format.parseDateTime("2014-01-01 08:18:00"), "123456.789", "queue1", "NONE", "CONNECT", None, None, None, None, None),
      QueueLog(4, format.parseDateTime("2014-01-01 08:20:00"), "123456.789", "queue2", "NONE", "ABANDON", None, None, None, None, None),
      QueueLog(5, format.parseDateTime("2014-01-01 08:21:00"), "123456.789", "queue2", "NONE", "JOINEMPTY", None, None, None, None, None),
      QueueLog(6, format.parseDateTime("2014-01-01 08:23:00"), "123456.789", "NONE", "Agent/12", "AGENTCALLBACKLOGOFF", None, None, None, None, None)
    )

    val loop = new CallEventsLoop(queueLogs, List(), List(new StatQueueManager(new Period(0, 15, 0, 0), 0)))
    loop.processNewCallEvents()

    StatQueuePeriodicTestUtils.allStatQueuePeriodic() should contain theSameElementsAs List(
      StatQueuePeriodic(None, format.parseDateTime("2014-01-01 08:00:00"), "queue1", 1, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0),
      StatQueuePeriodic(None, format.parseDateTime("2014-01-01 08:00:00"), "queue2", 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0),
      StatQueuePeriodic(None, format.parseDateTime("2014-01-01 08:15:00"), "queue1", 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
      StatQueuePeriodic(None, format.parseDateTime("2014-01-01 08:15:00"), "queue2", 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0)
    )
  }
}

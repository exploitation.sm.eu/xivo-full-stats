package xivo.fullstats.integration

import xivo.fullstats.CallEventsLoop
import xivo.fullstats.callparsers.CallDataGenerator
import xivo.fullstats.model._
import xivo.fullstats.testutils.{DBTest, DBUtil}

class CallDataIntegrationSpec extends DBTest(List("attached_data", "hold_periods", "call_data", "agent_position", "call_element")) {

  var loop: CallEventsLoop = null
  DBUtil.insertFromCsv()

  override def beforeEach() {
    super.beforeEach()
    loop = new CallEventsLoop(CelTestUtils.eventsByAscendingId(), List(new CallDataGenerator), List())
  }

  "A CallDataGenerator" should "retrieve uniqueid, src_num, src_num, dst_num and call_direction for each call" in {
    List(
      AgentPosition("1000", "2027", None, format.parseDateTime("2014-01-01 08:00:00"), None),
      AgentPosition("1001", "2015", Some("52015"), format.parseDateTime("2014-01-01 08:00:00"), None),
      AgentPosition("5000", "1000", None, format.parseDateTime("2015-05-05 08:00:00"), None)).foreach( AgentPosition.insertOrUpdate)
    loop.processNewCallEvents()
    val res = CallDataTestUtils.allCallData
    res.size shouldEqual 14
    for (callData <- res) {
      callData.uniqueId match {
        case "1392999905.102" => callData.dstNum shouldEqual Some("3059")
          callData.callDirection shouldEqual CallDirection.Internal
          callData.srcNum shouldEqual Some("2027")
          callData.srcAgent shouldEqual Some("1000")
          callData.dstAgent shouldEqual None

        case "1392999921.106" => callData.dstNum shouldEqual Some("3059")
          callData.callDirection shouldEqual CallDirection.Internal
          callData.srcNum shouldEqual Some("2027")
          callData.srcAgent shouldEqual Some("1000")
          callData.dstAgent shouldEqual None

        case "1392999944.110" => callData.dstNum shouldEqual Some("3059")
          callData.callDirection shouldEqual CallDirection.Internal
          callData.srcNum shouldEqual Some("2027")
          callData.srcAgent shouldEqual Some("1000")
          callData.dstAgent shouldEqual None

        case "1392999983.117" => callData.dstNum shouldEqual Some("2015")
          callData.callDirection shouldEqual CallDirection.Internal
          callData.srcNum shouldEqual Some("2027")
          callData.srcAgent shouldEqual Some("1000")
          callData.dstAgent shouldEqual Some("1001")

        case "1393000000.119" => callData.dstNum shouldEqual Some("2015")
          callData.callDirection shouldEqual CallDirection.Internal
          callData.srcNum shouldEqual Some("2027")
          callData.srcAgent shouldEqual Some("1000")
          callData.dstAgent shouldEqual Some("1001")

        case "1393000037.121" => callData.dstNum shouldEqual Some("3059")
          callData.callDirection shouldEqual CallDirection.Internal
          callData.srcNum shouldEqual Some("2027")
          callData.srcAgent shouldEqual Some("1000")
          callData.dstAgent shouldEqual None

        case "1393000060.125" => callData.dstNum shouldEqual Some("3046")
          callData.callDirection shouldEqual CallDirection.Internal
          callData.srcNum shouldEqual Some("2015")
          callData.srcAgent shouldEqual Some("1001")
          callData.dstAgent shouldEqual None

        case "1395391644.0" => callData.dstNum shouldEqual Some("**9844203")
          callData.callDirection shouldEqual CallDirection.Outgoing
          callData.srcNum shouldEqual Some("2015")
          callData.srcAgent shouldEqual Some("1001")
          callData.dstAgent shouldEqual None

        case "1395391699.2" => callData.dstNum shouldEqual Some("52015")
          callData.callDirection shouldEqual CallDirection.Incoming
          callData.srcNum shouldEqual Some("0230210082")
          callData.srcAgent shouldEqual None
          callData.dstAgent shouldEqual Some("1001")

        case "1395412138.6" => callData.dstNum shouldEqual Some("**9844203")
          callData.callDirection shouldEqual CallDirection.Outgoing
          callData.srcNum shouldEqual Some("2015")
          callData.srcAgent shouldEqual Some("1001")
          callData.dstAgent shouldEqual None

        case "1415020101.2" => callData.dstNum shouldEqual Some("399")
          callData.callDirection shouldEqual CallDirection.Internal
          callData.srcNum shouldEqual Some("310")
          callData.dstAgent shouldEqual None
          callData.srcAgent shouldEqual None

        case "1442495104.182" => callData.dstNum shouldEqual Some("0230210082")
          callData.callDirection shouldEqual CallDirection.Outgoing
          callData.srcNum shouldEqual Some("1002")
          callData.dstAgent shouldEqual None
          callData.srcAgent shouldEqual None

        case "1442495627.188" => callData.dstNum shouldEqual Some("0230210082")
          callData.callDirection shouldEqual CallDirection.Outgoing
          callData.srcNum shouldEqual Some("1002")
          callData.dstAgent shouldEqual None
          callData.srcAgent shouldEqual None

        case "1437737413.232" =>

        case _ => fail("We should not find another call : " + callData.uniqueId)
      }
    }

  }

  it should "retrieve the start time, answer time, end time and answer status" in {
    loop.processNewCallEvents()
    val res = CallDataTestUtils.allCallData
    val callDatas = res.map(cd => cd.copy(startTime = cd.startTime.withMillisOfSecond(0),
      endTime = cd.endTime.map(_.withMillisOfSecond(0)), answerTime = cd.answerTime.map(_.withMillisOfSecond(0))))
    callDatas.size shouldEqual 14
    for (callData <- callDatas) {
      callData.uniqueId match {
        case "1392999905.102" => callData.startTime shouldEqual format.parseDateTime("2014-02-21 17:25:05")
          callData.answerTime shouldEqual Some(format.parseDateTime("2014-02-21 17:25:11"))
          callData.endTime shouldEqual Some(format.parseDateTime("2014-02-21 17:25:15"))
          callData.status shouldEqual Some("answer")

        case "1392999921.106" => callData.startTime shouldEqual format.parseDateTime("2014-02-21 17:25:21")
          callData.answerTime shouldEqual None
          callData.endTime shouldEqual Some(format.parseDateTime("2014-02-21 17:25:37"))

        case "1392999944.110" => callData.startTime shouldEqual format.parseDateTime("2014-02-21 17:25:44")
          callData.answerTime shouldEqual Some(format.parseDateTime("2014-02-21 17:26:07"))
          callData.endTime shouldEqual Some(format.parseDateTime("2014-02-21 17:26:13"))
          callData.status shouldEqual Some("answer")

        case "1392999983.117" => callData.startTime shouldEqual format.parseDateTime("2014-02-21 17:26:23")
          callData.answerTime shouldEqual Some(format.parseDateTime("2014-02-21 17:26:28"))
          callData.endTime shouldEqual Some(format.parseDateTime("2014-02-21 17:26:31"))
          callData.status shouldEqual Some("answer")

        case "1393000000.119" => callData.startTime shouldEqual format.parseDateTime("2014-02-21 17:26:40")
          callData.answerTime should equal(None)
          callData.endTime shouldEqual Some(format.parseDateTime("2014-02-21 17:26:49"))

        case "1393000037.121" => callData.startTime shouldEqual format.parseDateTime("2014-02-21 17:27:17")
          callData.answerTime shouldEqual Some(format.parseDateTime("2014-02-21 17:27:21"))
          callData.endTime shouldEqual Some(format.parseDateTime("2014-02-21 17:28:13"))
          callData.status shouldEqual Some("answer")

        case "1393000060.125" => callData.startTime shouldEqual format.parseDateTime("2014-02-21 17:27:40")
          callData.answerTime should equal(None)
          callData.endTime shouldEqual Some(format.parseDateTime("2014-02-21 17:27:52"))

        case "1442495104.182" => callData.startTime shouldEqual format.parseDateTime("2015-09-17 15:05:04")
          callData.answerTime shouldEqual Some(format.parseDateTime("2015-09-17 15:05:07"))
          callData.endTime shouldEqual Some(format.parseDateTime("2015-09-17 15:05:08"))

        case "1442495627.188" => callData.startTime shouldEqual format.parseDateTime("2015-09-17 15:13:47")
          callData.answerTime shouldEqual None
          callData.endTime shouldEqual Some(format.parseDateTime("2015-09-17 15:13:56"))

        case "1395391644.0" | "1395391699.2" | "1395412138.6" | "1415020101.2" | "1437737413.232" =>
        case _ => fail("We should not find another call : " + callData.uniqueId)
      }
    }
  }

  it should "extract the ring on answer duration" in {
    loop.processNewCallEvents()
    val res = CallDataTestUtils.allCallData
    res.size shouldEqual 14
    for (callData <- res) {
      callData.uniqueId match {
        case "1392999905.102" => callData.ringDurationOnAnswer shouldEqual Some(5)
        case "1392999921.106" => callData.ringDurationOnAnswer shouldEqual None
        case "1392999944.110" => callData.ringDurationOnAnswer shouldEqual Some(2)
        case "1392999983.117" => callData.ringDurationOnAnswer shouldEqual Some(4)
        case "1393000000.119" => callData.ringDurationOnAnswer shouldEqual None
        case "1393000037.121" => callData.ringDurationOnAnswer shouldEqual Some(2)
        case "1393000060.125" => callData.ringDurationOnAnswer shouldEqual None
        case "1442495104.182" => callData.ringDurationOnAnswer shouldEqual Some(2)
        case "1442495627.188" => callData.ringDurationOnAnswer shouldEqual None
        case "1395391644.0" | "1395391699.2" | "1395412138.6" | "1415020101.2" | "1437737413.232" =>
        case _ => fail("We should not find another call : " + callData.uniqueId)
      }
    }
  }

  it should "put a flag on a call if it has been transfered" in {
    loop.processNewCallEvents()
    val res = CallDataTestUtils.allCallData
    res.size shouldEqual 14
    for (callData <- res) {
      callData.uniqueId match {
        case "1393000037.121" | "1437737413.232" => callData.transfered shouldBe true
        case "1392999905.102" | "1392999921.106" | "1392999944.110" | "1392999983.117" | "1415020101.2" | "1393000000.119"
             | "1393000060.125" | "1395391644.0" | "1395391699.2" | "1395412138.6" | "1442495104.182" | "1442495627.188" => callData.transfered shouldBe false
        case _ => fail("We should not find another call : " + callData.uniqueId)
      }
    }
  }

  it should "extract the attached data" in {
    loop.processNewCallEvents()
    val res = CallDataTestUtils.allCallData
    res.size shouldEqual 14
    for (callData <- res) {
      callData.uniqueId match {
        case "1392999905.102" => callData.attachedData shouldEqual List(AttachedData("test", "bonjour"))
        case "1437737413.232" => callData.attachedData shouldEqual List(AttachedData("recording", "xivo-1437737413.232"))
        case "1393000037.121" | "1392999921.106" | "1392999944.110" | "1392999983.117" | "1415020101.2" | "1393000000.119"
          | "1393000060.125" | "1395391644.0" | "1395391699.2" | "1395412138.6" | "1442495104.182" | "1442495627.188"  => callData.attachedData.size shouldEqual 0
        case _ => fail("We should not find another call : " + callData.uniqueId)
      }
    }
  }

  it should "extract the hold periods" in {
    loop.processNewCallEvents()
    val res = CallDataTestUtils.allCallData
    val callDatas = res.map(cd => cd.copy(holdPeriods = cd.holdPeriods.map(hp =>
      hp.copy(start = hp.start.withMillisOfSecond(0), end = hp.end.map(_.withMillisOfSecond(0))))))
    callDatas.size shouldEqual 14
    for (callData <- callDatas) {
      callData.uniqueId match {
        case "1415020101.2" => callData.holdPeriods shouldEqual List(
          HoldPeriod(format.parseDateTime("2014-11-03 14:08:30"), Some(format.parseDateTime("2014-11-03 14:08:33")), "1415020101.2"))
        case "1437737413.232" => callData.holdPeriods shouldEqual List(
          HoldPeriod(format.parseDateTime("2015-07-24 13:30:29"), Some(format.parseDateTime("2015-07-24 13:30:35")), "1437737413.232"))
        case "1393000037.121" | "1392999921.106" | "1392999944.110" | "1392999983.117" | "1392999905.102" | "1393000000.119"
             | "1393000060.125" | "1395391644.0" | "1395391699.2" | "1395412138.6" | "1442495104.182" | "1442495627.188" => callData.holdPeriods.size shouldEqual 0
        case _ => fail("We should not find another call : " + callData.uniqueId)
      }
    }
  }

  it should "extract call elements" in {
    loop.processNewCallEvents()
    for(callData <- CallDataTestUtils.allCallData) {
      callData.uniqueId match {
        case "1437737413.232" => CallElementTestUtils.byCallDataId(callData.id.get).map(_.copy(id=None)) shouldEqual List(
          CallElement(None, callData.id.get, formatMs.parseDateTime("2015-07-24 13:30:15.912"), Some(formatMs.parseDateTime("2015-07-24 13:30:22.302")),
            Some(formatMs.parseDateTime("2015-07-24 13:30:35.973")), "SIP/ql8pjr", None),
          CallElement(None, callData.id.get, formatMs.parseDateTime("2015-07-24 13:30:15.935"), None, Some(formatMs.parseDateTime("2015-07-24 13:30:22.305")),
            "SIP/6m7sqh", None),
          CallElement(None, callData.id.get, formatMs.parseDateTime("2015-07-24 13:30:36.209"), Some(formatMs.parseDateTime("2015-07-24 13:30:39.967")),
            Some(formatMs.parseDateTime("2015-07-24 13:30:43.199")), "SIP/6m7sqh", None))
        case _ =>
      }
    }
  }
}

package xivo.fullstats.integration

import xivo.fullstats.CallEventsLoop
import xivo.fullstats.agent.{AgentPositionManager, AgentPositionTestUtils}
import xivo.fullstats.model.{AgentPosition, QueueLog}
import xivo.fullstats.testutils.DBTest

class AgentPositionIntegrationSpec extends DBTest(List("queue_log", "agent_position", "extensions", "dialaction", "last_queue_log_id")) {
  "An AgentPositionManager" should "calculate agent positons" in {
    val pos1 = AgentPosition("1000", "2000", None, format.parseDateTime("2014-01-01 08:00:00"), Some(format.parseDateTime("2014-01-01 18:00:00")))
    val pos2 = AgentPosition("1001", "2001", None, format.parseDateTime("2014-01-01 08:00:00"), None)
    val login1000 = QueueLog(2, format.parseDateTime("2014-01-02 08:00:00"), "NONE", "", "Agent/1000", "AGENTCALLBACKLOGIN", Some("2002@default"), None, None, None, None)
    val login1002 = QueueLog(3, format.parseDateTime("2014-01-02 08:00:00"), "NONE", "", "Agent/1002", "AGENTCALLBACKLOGIN", Some("2004@default"), None, None, None, None)
    val logoff1001 = QueueLog(4, format.parseDateTime("2014-01-02 18:00:00"), "NONE", "", "Agent/1001", "AGENTCALLBACKLOGOFF", None, None, None, None, None)

    List(pos1, pos2).foreach(AgentPosition.insertOrUpdate)

    val loop = new CallEventsLoop(List(login1000, login1002, logoff1001), List(), List(new AgentPositionManager()))
    loop.processNewCallEvents()

    AgentPositionTestUtils.allAgentPositions() should contain theSameElementsAs  List(
      pos1,
      AgentPosition("1001", "2001", None, format.parseDateTime("2014-01-01 08:00:00"), Some(format.parseDateTime("2014-01-02 18:00:00"))),
      AgentPosition("1000", "2002", None, format.parseDateTime("2014-01-02 08:00:00"), None),
      AgentPosition("1002", "2004", None, format.parseDateTime("2014-01-02 08:00:00"), None))
  }

  it should "calculate agent positions with the SDA" in {
    AgentPositionTestUtils.createSdaForLine("0213457896", "2000")
    val ql = QueueLog(3, format.parseDateTime("2014-01-02 08:00:00"), "NONE", "", "Agent/1002", "AGENTCALLBACKLOGIN", Some("2000@default"), None, None, None, None)

    val loop = new CallEventsLoop(List(ql), List(), List(new AgentPositionManager()))
    loop.processNewCallEvents()

    AgentPositionTestUtils.allAgentPositions() shouldEqual List(AgentPosition("1002", "2000", Some("0213457896"), format.parseDateTime("2014-01-02 08:00:00"), None))
  }
}

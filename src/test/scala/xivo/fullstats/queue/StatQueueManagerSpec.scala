package xivo.fullstats.queue

import org.joda.time.Period
import org.scalatest.mock.EasyMockSugar
import xivo.fullstats.model.{QueueLog, StatQueuePeriodicTestUtils, StatQueuePeriodic}
import xivo.fullstats.testutils.DBTest

class StatQueueManagerSpec extends DBTest(List("call_on_queue", "stat_queue_periodic")) with EasyMockSugar {

  // intervalle de 15 minutes (heures, minutes, secondes, millisecondes)
  val interval = new Period(0, 15, 0, 0)
  var manager: StatQueueManager = null

  override def beforeEach() {
    super.beforeEach()
    manager = new StatQueueManager(interval, 0)
  }

  "A StatQueueManager" should "save the current StatQueuePeriodic" in {
    manager.queues.put("test1",
      new StatQueueComputer("test1", StatQueuePeriodic(None, format.parseDateTime("2014-01-01 08:00:00"), "test1", 2, 4, 6, 3, 0, 0, 1, 4, 7, 0, 0), interval))
    manager.queues.put("test2",
      new StatQueueComputer("test2", StatQueuePeriodic(None, format.parseDateTime("2014-01-01 08:00:00"), "test2", 1,6, 3, 0, 1, 1, 2, 0, 2, 1, 0), interval))

    manager.saveState()

    StatQueuePeriodicTestUtils.allStatQueuePeriodic() shouldEqual List(
      StatQueuePeriodic(None, format.parseDateTime("2014-01-01 08:00:00"), "test2", 1,6, 3, 0, 1, 1, 2, 0, 2, 1, 0),
        StatQueuePeriodic(None, format.parseDateTime("2014-01-01 08:00:00"), "test1", 2, 4, 6, 3, 0, 0, 1, 4, 7, 0, 0)
    )
  }

  it should "transmit the QueueLog to the appropriate StatQueueComputer" in {
    val computer1 = mock[StatQueueComputer]
    val computer2 = mock[StatQueueComputer]
    manager.queues.put("queue1", computer1)
    manager.queues.put("queue2", computer2)
    val ql = QueueLog(1, format.parseDateTime("2014-01-01 08:00:00"), "123456.789", "queue2", "NONE", "ENTERQUEUE", None, None, None, None, None)
    computer2.processQueueLog(ql)

    whenExecuting(computer1, computer2) {
      manager.parseEvent(ql)
    }
  }

  it should "not process a QueueLog if its id is lesser than the provided one" in {
    manager = new StatQueueManager(interval, 10)
    val computer= mock[StatQueueComputer]
    manager.queues.put("queue1", computer)
    val ql1 = QueueLog(8, format.parseDateTime("2014-01-01 08:00:00"), "12346.789", "queue1", "NONE", "ENTERQUEUE", None, None, None, None, None)
    val ql2 = QueueLog(10, format.parseDateTime("2014-01-01 08:01:00"), "12346.789", "queue1", "NONE", "ENTERQUEUE", None, None, None, None, None)
    val ql3 = QueueLog(11, format.parseDateTime("2014-01-01 08:02:00"), "12346.789", "queue1", "NONE", "ENTERQUEUE", None, None, None, None, None)
    computer.processQueueLog(ql2)
    computer.processQueueLog(ql3)

    whenExecuting(computer) {
      manager.parseEvent(ql1)
      manager.parseEvent(ql2)
      manager.parseEvent(ql3)
    }
  }

  it should "process only queue logs whose event is usefull, but still set the currentInterval" in {
    val usefulEvents = List("ENTERQUEUE", "CLOSED", "FULL", "DIVERT_CA_RATIO", "DIVERT_HOLDTIME", "JOINEMPTY",  "ABANDON",
      "CONNECT", "EXITWITHTIMEOUT", "LEAVEEMPTY", "EXITWITHKEY")
    val ql = QueueLog(8, format.parseDateTime("2014-01-01 08:00:00"), "12346.789", "queue1", "NONE", "", None, None, None, None, None)
    val validQueueLogs = usefulEvents.map(e => ql.copy(event=e))
    val allQueueLogs = ql.copy(event="AGENTCALLBACKLOGIN") :: validQueueLogs
    val computer= mock[StatQueueComputer]
    manager.queues.put("queue1", computer)
    validQueueLogs.foreach(qlog => computer.processQueueLog(qlog))

    whenExecuting(computer) {
      allQueueLogs.foreach(manager.parseEvent)
      manager.currentInterval shouldEqual Some(format.parseDateTime("2014-01-01 08:00:00"))
    }
  }

  it should "flush all the computers if the provided queue log is in a new period" in {
    val c1 = mock[StatQueueComputer]
    val c2 = mock[StatQueueComputer]
    manager.queues.put("queue1", c1)
    manager.queues.put("queue2", c2)
    val sqp1 = StatQueuePeriodic(None, format.parseDateTime("2014-01-01 08:00:00"), "queue1", 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0)
    manager.currentInterval = Some(format.parseDateTime("2014-01-01 08:15:00"))

    val qlDate = format.parseDateTime("2014-01-01 08:31:00")
    val ql = QueueLog(8, qlDate, "12346.789", "queue1", "AGENTCALLBACKLOGIN", "", None, None, None, None, None)
    c1.notifyNewInterval(qlDate).andReturn(Some(sqp1))
    c2.notifyNewInterval(qlDate).andReturn(None)

    whenExecuting(c1, c2) {
      manager.parseEvent(ql)
      StatQueuePeriodicTestUtils.allStatQueuePeriodic() shouldEqual List(sqp1)
      manager.currentInterval shouldEqual Some(format.parseDateTime("2014-01-01 08:30:00"))
    }
  }
}

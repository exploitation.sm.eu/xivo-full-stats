package xivo.fullstats.callparsers.cel

import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import xivo.fullstats.model.HoldPeriod

class TestHangedUpState extends TestState {

  val format = DateTimeFormat.forPattern("YYYY-MM-dd HH:mm:ss")

  "The HangedUp state" should "throw an exception when it processes a Cel" in {
    val hangup = new HangedUp(callData, cel)
    intercept[InvalidCelException] {
      hangup.processCel(cel)
    }
  }
  
  it should "set the endTime" in {
    cel.eventTime = format.parseDateTime("2014-05-23 14:05:23")
    
    new HangedUp(callData, cel)
    
    callData.endTime shouldEqual Some(format.parseDateTime("2014-05-23 14:05:23"))
  }

  it should "close the current hold period" in {
    callData.addHoldPeriod(HoldPeriod(format.parseDateTime("2014-01-01 08:15:45"), Some(format.parseDateTime("2014-01-01 08:16:00")), cel.linkedId))
    callData.addHoldPeriod(HoldPeriod(format.parseDateTime("2014-01-01 08:17:45"), None, cel.linkedId))
    cel.eventTime = format.parseDateTime("2014-01-01 08:18:00")

    new HangedUp(callData, cel)

    callData.holdPeriods shouldEqual List(HoldPeriod(format.parseDateTime("2014-01-01 08:17:45"), Some(cel.eventTime), cel.linkedId),
      HoldPeriod(format.parseDateTime("2014-01-01 08:15:45"), Some(format.parseDateTime("2014-01-01 08:16:00")), cel.linkedId))
  }

  it should "not modify an already closed hold period" in {
    callData.addHoldPeriod(HoldPeriod(format.parseDateTime("2014-01-01 08:15:45"), Some(format.parseDateTime("2014-01-01 08:16:00")), cel.linkedId))
    callData.addHoldPeriod(HoldPeriod(format.parseDateTime("2014-01-01 08:17:45"), Some(format.parseDateTime("2014-01-01 08:18:00")), cel.linkedId))
    cel.eventTime = format.parseDateTime("2014-01-01 08:18:30")

    new HangedUp(callData, cel)

    callData.holdPeriods shouldEqual List(HoldPeriod(format.parseDateTime("2014-01-01 08:17:45"), Some(format.parseDateTime("2014-01-01 08:18:00")), cel.linkedId),
      HoldPeriod(new DateTime(format.parseDateTime("2014-01-01 08:15:45")), Some(format.parseDateTime("2014-01-01 08:16:00")), cel.linkedId))
  }
  
}
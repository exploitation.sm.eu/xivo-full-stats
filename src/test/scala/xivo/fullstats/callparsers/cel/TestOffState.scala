package xivo.fullstats.callparsers.cel

import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat

class TestOffState extends TestState {

  val format = DateTimeFormat.forPattern("YYYY-MM-dd HH:mm:ss")

  "The Off state" should "return Started on CHAN_START and set the uniqueid, dstNum, srcNum, startTime and srcInterface" in {
    cel.eventType = "CHAN_START"
    cel.exten = "1000"
    cel.context = "default"
    cel.linkedId = "123456.789"
    cel.eventTime = format.parseDateTime("2014-05-23 14:05:23")
    cel.cidNum = "1005"
    cel.chanName = "Local/my-trunk-test-01263546"
    val off = new Off(callData)

    off.processCel(cel).isInstanceOf[Started] shouldBe true
    callData.dstNum shouldEqual Some("1000")
    callData.uniqueId shouldEqual "123456.789"
    callData.startTime shouldEqual new DateTime(format.parseDateTime("2014-05-23 14:05:23"))
    callData.srcNum shouldEqual Some("1005")
    callData.srcInterface shouldEqual Some("Local/my-trunk-test")
  }

  it should "not set the dstNum if the exten equals to 's'" in {
    cel.eventType = "CHAN_START"
    cel.exten = "s"
    val off = new Off(callData)

    off.processCel(cel).isInstanceOf[Started] shouldBe true
    callData.dstNum shouldEqual None
  }

  it should "throw an exception otherwise" in {
    cel.eventType = "ANSWER"
    val off = new Off(callData)
    intercept[InvalidCelException] {
      off.processCel(cel)
    }
  }
}
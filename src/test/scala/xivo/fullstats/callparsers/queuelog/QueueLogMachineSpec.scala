package xivo.fullstats.callparsers.queuelog

import org.scalatest.mock.EasyMockSugar
import xivo.fullstats.EmptyObjectProvider
import xivo.fullstats.testutils.DBTest

class QueueLogMachineSpec extends DBTest(List("call_on_queue")) with EasyMockSugar {

  var machine: QueueLogMachine = null

  override def beforeEach(): Unit = {
    super.beforeEach()
    machine = new QueueLogMachine()
    machine.state = mock[QueueLogState]
  }

  "QueueLogMachine.forceCloture" should "set the hangup time equal to answer time if defined" in {
    val cq = EmptyObjectProvider.emptyCallOnQueue()
    cq.queueTime = format.parseDateTime("2014-01-01 08:00:00")
    cq.answerTime = Some(format.parseDateTime("2014-01-01 08:01:00"))
    machine.state.getResult.andReturn(cq)

    whenExecuting(machine.state) {
      machine.forceCloture()
      cq.hangupTime shouldEqual cq.answerTime
    }
  }

  it should "set the end time equal to start time if answer time not defined" in {
    val cq = EmptyObjectProvider.emptyCallOnQueue()
    cq.queueTime = format.parseDateTime("2014-01-01 08:00:00")
    machine.state.getResult.andReturn(cq)

    whenExecuting(machine.state) {
      machine.forceCloture()
      cq.hangupTime shouldEqual Some(cq.queueTime)
    }
  }

  it should "do nothing if the hangup date is set" in {
    val cq = EmptyObjectProvider.emptyCallOnQueue()
    cq.hangupTime = Some(format.parseDateTime("2015-01-01 08:10:00"))
    cq.queueTime = format.parseDateTime("2015-01-01 08:00:00")
    machine.state.getResult.andReturn(cq)

    whenExecuting(machine.state) {
      machine.forceCloture()
      cq.hangupTime shouldEqual Some(format.parseDateTime("2015-01-01 08:10:00"))
    }
  }

}

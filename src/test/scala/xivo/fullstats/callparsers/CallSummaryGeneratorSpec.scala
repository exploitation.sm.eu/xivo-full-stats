package xivo.fullstats.callparsers

import java.sql.Connection

import org.joda.time.DateTime
import org.scalatest.mock.EasyMockSugar
import org.scalatest.{FlatSpec, Matchers}
import xivo.fullstats.callparsers.queuelog.QueueLogMachine
import xivo.fullstats.model.{CallEvent, CallSummary, QueueLog}

class CallSummaryGeneratorSpec extends FlatSpec with Matchers with EasyMockSugar {

  "An CallSummaryGenerator" should "call the appropriate state machine for all events" in {
    val stateMachine1 = mock[StateMachine[CallEvent, CallSummary]]
    val stateMachine2 = mock[StateMachine[CallEvent, CallSummary]]
    val callid1 = "123456.788"
    val callid2 = "123456.789"
    val now = new DateTime()
    val (e1, e2, e3, e4) = (niceMock[CallEvent], niceMock[CallEvent], niceMock[CallEvent], niceMock[CallEvent])
    e1.callid.andReturn(callid1).anyTimes()
    e1.time.andReturn(now).anyTimes()
    e2.callid.andReturn(callid2).anyTimes()
    e2.time.andReturn(now).anyTimes()
    e3.callid.andReturn(callid1).anyTimes()
    e3.time.andReturn(now).anyTimes()
    e4.callid.andReturn(callid2).anyTimes()
    e4.time.andReturn(now).anyTimes()

    val r1 = mock[CallSummary]
    stateMachine1.processEvent(e1)
    stateMachine1.isCallFinished.andReturn(false)
    stateMachine1.processEvent(e3)
    stateMachine1.isCallFinished.andReturn(true)
    stateMachine1.getResult.andReturn(Some(r1))
    stateMachine2.processEvent(e2)
    stateMachine2.isCallFinished.andReturn(false)
    stateMachine2.processEvent(e4)
    stateMachine2.isCallFinished.andReturn(false)

    val machineFactory = mock[() => StateMachine[CallEvent, CallSummary]]
    machineFactory.apply().andReturn(stateMachine1).andReturn(stateMachine2)

    val postProcessFunc = mock[CallSummary => Unit]
    postProcessFunc.apply(r1).andReturn(Unit)

    val saveFunc = mock[Iterable[CallSummary] => Unit]
    saveFunc.apply(Some(r1)).andReturn(Unit)

    whenExecuting(e1, e2, e3, e4, stateMachine1, stateMachine2, machineFactory, postProcessFunc, saveFunc)({
      val parser = new CallSummaryGenerator(machineFactory, Some(postProcessFunc)){
        override def saveResult(res: Iterable[CallSummary]): Unit = saveFunc(res)
      }
      List(e1, e2, e3, e4).foreach(parser.parseEvent)
    })
  }

  it should "call the postProcessing method and flush the map" in {
    val c1 = mock[CallSummary]
    val c2 = mock[CallSummary]
    val m1 = mock[StateMachine[CallEvent, CallSummary]]
    val m2 = mock[StateMachine[CallEvent, CallSummary]]
    val postProcessing = mock[CallSummary => Unit]
    val saveMethod = mock[Iterable[CallSummary] => Unit]
    val parser = new CallSummaryGenerator(() => m1, Some(postProcessing)){
      override def saveResult(res: Iterable[CallSummary]): Unit = saveMethod(res)
    }
    parser.callMap.put("123456.788", m1)
    parser.callMap.put("123456.789", m2)

    m1.getResult.andReturn(Some(c1))
    m2.getResult.andReturn(Some(c2))
    postProcessing.apply(c1).andReturn(Unit)
    postProcessing.apply(c2).andReturn(Unit)
    saveMethod.apply(List(c2, c1)).andReturn(Unit)

    whenExecuting(m1, m2, postProcessing, saveMethod) {
      parser.saveState()
    }
  }

  it should "initialize the lastPurgeDate" in {
    val m1 = mock[StateMachine[CallEvent, CallSummary]]
    val parser = new CallSummaryGenerator(() => m1, None) {
      override def saveResult(res: Iterable[CallSummary]): Unit = ???
    }
    val date = new DateTime()
    val e = mock[CallEvent]
    e.time.andReturn(date).anyTimes()

    whenExecuting(e) {
      parser.purgeIfRequired(e)
      parser.lastPurgeDate shouldEqual Some(date)
    }
  }

  it should "remove old calls from the map, cloture them and save them" in {
    val stateMachine1 = mock[StateMachine[CallEvent, CallSummary]]
    val stateMachine2 = mock[StateMachine[CallEvent, CallSummary]]
    val callid1 = "123456.788"
    val callid2 = "123456.789"
    val now = DateTime.now()
    val lastPurgeDate = now.minusDays(1).minusSeconds(1)
    val d1 = lastPurgeDate.minusSeconds(1)
    val d2 = lastPurgeDate.plusHours(1)
    val e1 = mock[CallEvent]
    e1.callid.andReturn(callid1).anyTimes()
    e1.time.andReturn(now).anyTimes()
    val r1 = mock[CallSummary]
    val r2 = mock[CallSummary]
    stateMachine1.processEvent(e1)
    stateMachine1.isCallFinished.andReturn(false)
    stateMachine1.getResult.andReturn(Some(r1)).anyTimes()
    r1.getStartTime.andReturn(d1)
    stateMachine2.getResult.andReturn(Some(r2)).anyTimes()
    r2.getStartTime.andReturn(d2)

    val machineFactory = mock[() => StateMachine[CallEvent, CallSummary]]

    stateMachine1.forceCloture()
    val saveFunc = mock[Iterable[CallSummary] => Unit]
    saveFunc.apply(Some(r1)).andReturn(Unit)

    val parser = new CallSummaryGenerator(machineFactory, None){
      override def saveResult(res: Iterable[CallSummary]): Unit = saveFunc(res)
    }
    parser.callMap.put(callid1, stateMachine1)
    parser.callMap.put(callid2, stateMachine2)
    parser.lastPurgeDate = Some(lastPurgeDate)

    whenExecuting(e1, r1, r2, stateMachine1, stateMachine2, machineFactory, saveFunc)({
      parser.parseEvent(e1)
      parser.lastPurgeDate shouldEqual Some(now)
      parser.callMap shouldEqual Map(callid2 -> stateMachine2)
    })
  }

  "A CallOnQueueGenerator" should "parse an event only if it is in the list of valid ones" in {
    implicit val c = mock[Connection]

    val validEvents = List("ENTERQUEUE", "CONNECT", "COMPLETEAGENT", "COMPLETECALLER", "TRANSFER", "ABANDON", "EXITWITHKEY",
      "EXITWITHTIMEOUT", "CLOSED", "DIVERT_CA_RATIO", "DIVERT_HOLDTIME", "FULL", "LEAVEEMPTY", "JOINEMPTY", "RINGNOANSWER")
    val ql = QueueLog(0, new DateTime(), "123456.789", "queue", "NONE", "", None, None, None, None, None)
    val validQueueLogs = validEvents.map(e => ql.copy(event=e))
    val allQueueLogs = ql.copy(event="AGENTCALLBACKLOGIN")::validQueueLogs
    val generator = new CallOnQueueGenerator()
    val fsm = mock[QueueLogMachine]
    generator.callMap.put(ql.callid, fsm)

    validQueueLogs.foreach(fsm.processEvent)
    validQueueLogs.foreach(ql => fsm.isCallFinished.andReturn(false))

    whenExecuting(fsm) {
      allQueueLogs.foreach(generator.parseEvent)
    }

  }
}

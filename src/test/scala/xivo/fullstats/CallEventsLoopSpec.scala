package xivo.fullstats

import java.sql.Connection

import org.scalatest.mock.EasyMockSugar
import org.scalatest.{FlatSpec, Matchers}
import xivo.fullstats.model.{Cel, QueueLog}

class CallEventsLoopSpec extends FlatSpec with Matchers with EasyMockSugar {

  "A CallEventLoop" should "process new call events" in {
    implicit val c = mock[Connection]
    val parserC1 = mock[EventParser[Cel]]
    val parserC2 = mock[EventParser[Cel]]
    val parserQ1 = mock[EventParser[QueueLog]]
    val parserQ2 = mock[EventParser[QueueLog]]
    val (q1, c1, q2, c2) = (mock[QueueLog], mock[Cel], mock[QueueLog], mock[Cel])

    parserC1.parseEvent(c1)
    parserC2.parseEvent(c1)
    parserC1.parseEvent(c2)
    parserC2.parseEvent(c2)

    parserQ1.parseEvent(q1)
    parserQ2.parseEvent(q1)
    parserQ1.parseEvent(q2)
    parserQ2.parseEvent(q2)

    parserQ1.saveState()
    parserQ2.saveState()
    parserC1.saveState()
    parserC2.saveState()

    whenExecuting(parserC1, parserC2, parserQ1, parserQ2) {
      new CallEventsLoop(List(c1, q1, c2, q2), List(parserC1, parserC2), List(parserQ1, parserQ2)).processNewCallEvents()
    }
  }
}

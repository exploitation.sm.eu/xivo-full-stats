package xivo.fullstats.model

import java.sql.Connection
import java.util.Date

import anorm.SqlParser._
import anorm._
import org.joda.time.DateTime
import xivo.fullstats.EmptyObjectProvider
import xivo.fullstats.testutils.DBTest

class CelSpec extends DBTest(List("cel")) {

  "The Cel singleton" should "return the linkedid associated to a uniqueid" in {
    CelTestUtils.insertCel(EmptyObjectProvider.emptyCel().copy(uniqueId="123456.789",linkedId="123456.777"))

    Cel.linkedIdFromUniqueId("123456.789") shouldEqual Some("123456.777")
    Cel.linkedIdFromUniqueId("12345.999") shouldEqual None
  }

  it should "override lastIdTable qith last_cel_id" in {
    Cel.lastIdTable shouldEqual "last_cel_id"
  }
}

object CelTestUtils {
  def insertCel(cel: Cel)(implicit c: Connection) {
    SQL("""INSERT INTO cel(id, uniqueid, linkedid, eventtype, eventtime, userdeftype, cid_name, cid_num, cid_ani, cid_rdnis, cid_dnid, exten, context, channame, appname, appdata, amaflags, accountcode, peeraccount, userfield, peer)
      VALUES ({id}, {uniqueid}, {linkedid}, {eventtype}, {eventtime}, '', '', '', '', '', '', '', '', '', '', '', 0, '', '', '', '')""")
      .on('id -> cel.id, 'uniqueid -> cel.uniqueId, 'linkedid -> cel.linkedId, 'eventtype -> cel.eventType, 'eventtime -> cel.eventTime.toDate)
      .executeUpdate
  }

  val simple = get[Int]("id") ~
        get[String]("eventtype") ~
        get[Date]("eventtime") ~
        get[String]("userdeftype") ~
        get[String]("cid_name") ~
        get[String]("cid_num") ~
        get[String]("cid_ani") ~
        get[String]("cid_rdnis") ~
        get[String]("cid_dnid") ~
        get[String]("exten") ~
        get[String]("context") ~
        get[String]("channame") ~
        get[String]("appname") ~
        get[String]("appdata") ~
        get[Int]("amaflags") ~
        get[String]("accountcode") ~
        get[String]("peeraccount") ~
        get[String]("uniqueid") ~
        get[String]("linkedid") ~
        get[String]("userfield") ~
        get[String]("peer") ~
        get[Option[Int]]("call_log_id") map {
        case id ~ eventtype ~ eventTime ~ userdeftype ~ cidname ~ cidnum ~ cidani ~ cidrdnis ~ cidDnid ~ exten ~
            context ~ channame ~ appname ~ appdata ~ amaflags ~ accountcode ~ peeraccount ~
            uniqueid ~ linkedid ~ userfield ~ peer ~ call_log_id =>
            Cel(id, eventtype, new DateTime(eventTime), userdeftype, cidname, cidnum, cidani, cidrdnis, cidDnid, exten,
                context, channame, appname, appdata, amaflags, accountcode, peeraccount,
                uniqueid, linkedid, userfield, peer, call_log_id)
      }

      def eventsByAscendingId()(implicit connection: Connection): List[Cel] = SQL(s"select * from cel order by id asc").as(simple *)


}
package xivo.fullstats.model

import java.sql.Connection

import anorm.SQL
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import xivo.fullstats.testutils.DBTest

class QueueLogSpec extends DBTest(List("queue_log")) {

  "The QueueLog singleton" should "override lastIdTable with last_queue_log_id" in {
    QueueLog.lastIdTable shouldEqual "last_queue_log_id"
  }

  it should "get the max date" in {
    val ql1 = QueueLog(1, format.parseDateTime("2013-01-01 08:00:00"), "123456.789", "queue", "NONE", "ENTERQUEUE", None, None, None, None, None)
    val ql2 = QueueLog(2, format.parseDateTime("2013-01-01 09:00:00"), "123456.789", "queue", "NONE", "ENTERQUEUE", None, None, None, None, None)
    val ql3 = QueueLog(3, format.parseDateTime("2013-01-01 10:00:00"), "123456.789", "queue", "NONE", "ENTERQUEUE", None, None, None, None, None)
    List(ql1, ql2, ql3).foreach(QueueLogTestUtils.insertQueueLog)

    QueueLog.getMaxDate() shouldEqual Some(format.parseDateTime("2013-01-01 10:00:00"))
  }

  it should "return None for the max date if there is no data" in {
    QueueLog.getMaxDate() shouldBe None
  }
}

object QueueLogTestUtils {
  val format = DateTimeFormat.forPattern("YYYY-MM-dd HH:mm:ss.SSS")
  def insertQueueLog(ql: QueueLog)(implicit conn: Connection) {
    SQL("""INSERT INTO queue_log(id, time, callid, queuename, agent, event, data1, data2, data3, data4, data5)
              VALUES ({id}, {time}, {callid}, {queuename}, {agent}, {event}, {data1}, {data2}, {data3}, {data4}, {data5})""").on(
        'id -> ql.id, 'time -> new DateTime(ql.time).toString(format), 'callid -> ql.callid, 'queuename -> ql.queueName, 'agent -> ql.agent, 'event -> ql.event,
        'data1 -> ql.data1, 'data2 -> ql.data2, 'data3 -> ql.data3, 'data4 -> ql.data4, 'data5 -> ql.data5).executeUpdate()
  }
}
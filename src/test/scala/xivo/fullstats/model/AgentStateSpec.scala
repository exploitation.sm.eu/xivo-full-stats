package xivo.fullstats.model

import java.sql.Connection
import java.util.Date
import anorm._
import anorm.SqlParser._

import org.joda.time.DateTime
import xivo.fullstats.testutils.DBTest

class AgentStateSpec extends DBTest(List("agent_states")) {

  "The AgentState singleton" should "insert the states" in {
    SQL("INSERT INTO agent_states(agent, time, state) VALUES ('1000', '2013-01-01 07:00:00', 'logged_off')").executeUpdate()
    val state1 = AgentState(format.parseDateTime("2013-01-01 08:00:03"), State.LoggedOn)
    val state2 = AgentState(format.parseDateTime("2013-01-01 09:10:03"), State.Paused)

    AgentState.replaceState("1000", state1)
    AgentState.replaceState("2000", state2)

    AgentStateTestUtils.getStates shouldEqual Map("1000" -> state1, "2000" -> state2)
  }

  it should "retrieve the state for an agent" in {
    SQL("INSERT INTO agent_states(agent, time, state) VALUES ('1000', '2013-01-01 07:00:00', 'logged_off')").executeUpdate()

    AgentState.lastForAgent("1000") shouldEqual Some(AgentState(format.parseDateTime("2013-01-01 07:00:00"), State.LoggedOff))
    AgentState.lastForAgent("2000") shouldEqual None
  }

}

object AgentStateTestUtils {
  def getStates()(implicit conn: Connection): Map[String, AgentState] =
    SQL("SELECT agent, time, state::varchar FROM agent_states").as((get[String]("agent") ~ get[Date]("time") ~ get[String]("state") map {
      case agent ~ time ~ state => agent -> AgentState(new DateTime(time), State.withName(state))
    }) *).toMap

  def countStates()(implicit c: Connection): Long = SQL("SELECT count(*) AS res FROM agent_states").as(get[Long]("res") *).head
}

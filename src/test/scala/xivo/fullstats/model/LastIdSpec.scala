package xivo.fullstats.model

import anorm.SqlParser._
import anorm._
import xivo.fullstats.testutils.DBTest

class LastIdSpec extends DBTest(List("last_cel_id")) {

  object LastCelId extends LastIdTable {
    override val lastIdTable: String = "last_cel_id"
  }

  "The LastTableId class" should "insert or create the last id row" in {
    LastCelId.setLastId(123)
    SQL("SELECT id FROM last_cel_id").as(get[Int]("id") *) shouldEqual List(123)
    LastCelId.setLastId(456)
    SQL("SELECT id FROM last_cel_id").as(get[Int]("id") *) shouldEqual List(456)
  }

  it should "retrieve the last id row" in {
    SQL("INSERT INTO last_cel_id(id) VALUES (123)").executeUpdate
    
    LastCelId.getLastId should equal(123)
  }
  
  it should "return 0 if no id is found" in {
    LastCelId.getLastId should equal(0)
  }

}
package xivo.fullstats.model

import java.sql.Connection
import java.util.Date
import anorm._
import anorm.SqlParser._
import org.joda.time.DateTime

import xivo.fullstats.testutils.DBTest

class CallElementSpec extends DBTest(List("call_element")) {

  val cd = CallData.insert(CallData(None, "12345.789", None, CallDirection.Incoming, new DateTime(), None, None, None, None, false, None, None))

  "The CallElement singleton" should "insert a call element in the database" in {
    val callElement = CallElement(None, cd.id.get, new DateTime, Some(new DateTime()), Some(new DateTime()), "SIP/test", Some("1000"))

    val res = CallElement.insert(callElement)

    res.id.isDefined shouldBe true
    res.copy(id=None) shouldEqual callElement
    CallElementTestUtils.allCallElements shouldEqual List(res)
  }

  it should "update a call element" in {
    val c1 = CallElement.insert(CallElement(None, cd.id.get, new DateTime, Some(new DateTime()), Some(new DateTime()), "SIP/test", Some("1000")))
    val c2 = CallElement.insert(CallElement(None, cd.id.get, new DateTime, Some(new DateTime()), Some(new DateTime()), "SIP/other", Some("1002")))
    val modifiedC2 = c2.copy(interface="SIP/crochnef")

    CallElement.update(modifiedC2)

    CallElementTestUtils.allCallElements shouldEqual List(c1, modifiedC2)
  }

}

object CallElementTestUtils {
  val simple = get[Option[Long]]("id") ~
    get[Long]("call_data_id") ~
    get[Date]("start_time") ~
    get[Option[Date]]("answer_time") ~
    get[Option[Date]]("end_time") ~
    get[String]("interface") ~
    get[Option[String]]("agent") map {
      case id ~ cdId ~ start ~ answer ~ end ~ interface ~ agent =>
        CallElement(id, cdId, new DateTime(start), answer.map(new DateTime(_)), end.map(new DateTime(_)), interface, agent)
    }

  def allCallElements(implicit c: Connection): List[CallElement] = SQL(
  "SELECT id, call_data_id, start_time, answer_time, end_time, interface, agent FROM call_element ORDER BY id ASC").as(simple *)

  def byCallDataId(id: Long)(implicit c: Connection) = SQL(
    "SELECT id, call_data_id, start_time, answer_time, end_time, interface, agent FROM call_element WHERE call_data_id = {id} ORDER BY id ASC")
    .on('id -> id).as(simple *)
}
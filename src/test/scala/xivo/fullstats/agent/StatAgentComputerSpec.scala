package xivo.fullstats.agent

import org.joda.time.Period
import xivo.fullstats.model._
import xivo.fullstats.testutils.DBTest

class StatAgentComputerSpec extends DBTest(List("stat_agent_periodic", "agent_states")) {

  val interval = new Period(0, 15, 0, 0)
  var computer: StatAgentComputer = null

  override def beforeEach(): Unit = {
    super.beforeEach()
    computer = new StatAgentComputer(
      "2000",
      AgentState(format.parseDateTime("2014-01-01 08:00:00"), State.LoggedOn),
      StatAgentPeriodic(None, format.parseDateTime("2014-01-01 08:00:00"), "2000", 0, 0, 0),
      interval)
  }

  "A StatAgentComputer" should "generate empty StatAgentPeriodic between two dates" in {
    computer.generatePeriods(format.parseDateTime("2014-01-01 08:07:00"), format.parseDateTime("2014-01-01 10:16:13"), "2000") shouldEqual
      List(StatAgentPeriodic(None, format.parseDateTime("2014-01-01 08:15:00"), "2000", 0, 0, 0),
        StatAgentPeriodic(None, format.parseDateTime("2014-01-01 08:30:00"), "2000", 0, 0, 0),
        StatAgentPeriodic(None, format.parseDateTime("2014-01-01 08:45:00"), "2000", 0, 0, 0),
        StatAgentPeriodic(None, format.parseDateTime("2014-01-01 09:00:00"), "2000", 0, 0, 0),
        StatAgentPeriodic(None, format.parseDateTime("2014-01-01 09:15:00"), "2000", 0, 0, 0),
        StatAgentPeriodic(None, format.parseDateTime("2014-01-01 09:30:00"), "2000", 0, 0, 0),
        StatAgentPeriodic(None, format.parseDateTime("2014-01-01 09:45:00"), "2000", 0, 0, 0),
        StatAgentPeriodic(None, format.parseDateTime("2014-01-01 10:00:00"), "2000", 0, 0, 0),
        StatAgentPeriodic(None, format.parseDateTime("2014-01-01 10:15:00"), "2000", 0, 0, 0))
  }

  it should "propagate a state from the state date until the end of a period" in {
    val state = AgentState(format.parseDateTime("2014-01-01 08:05:00"), State.LoggedOn)
    val period = StatAgentPeriodic(None, format.parseDateTime("2014-01-01 08:00:00"), "2000", 0, 0, 0)
    computer.propagateState(state, period, format.parseDateTime("2014-01-01 08:17:00"))
    period.loginTime shouldEqual 600
    period.pauseTime shouldEqual 0

    val state2 = AgentState(format.parseDateTime("2014-01-01 08:05:00"), State.Paused)
    val period2 = StatAgentPeriodic(None, format.parseDateTime("2014-01-01 08:00:00"), "2000", 0, 0, 0)
    computer.propagateState(state2, period2, format.parseDateTime("2014-01-01 08:17:00"))
    period2.loginTime shouldEqual 600
    period2.pauseTime shouldEqual 600
  }

  it should "propagate a state from the period start until the max date" in {
    val state = AgentState(format.parseDateTime("2014-01-01 07:55:00"), State.LoggedOn)
    val period = StatAgentPeriodic(None, format.parseDateTime("2014-01-01 08:00:00"), "2000", 0, 0, 0)
    computer.propagateState(state, period, format.parseDateTime("2014-01-01 08:10:00"))
    period.loginTime shouldEqual 600
    period.pauseTime shouldEqual 0

    val state2 = AgentState(format.parseDateTime("2014-01-01 07:55:00"), State.Paused)
    val period2 = StatAgentPeriodic(None, format.parseDateTime("2014-01-01 08:00:00"), "2000", 0, 0, 0)
    computer.propagateState(state2, period2, format.parseDateTime("2014-01-01 08:10:00"))
    period2.loginTime shouldEqual 600
    period2.pauseTime shouldEqual 600
  }

  it should "save its state" in {
    val computer = new StatAgentComputer("2000", AgentState(format.parseDateTime("2014-01-01 08:01:00"), State.Paused),
      StatAgentPeriodic(None, format.parseDateTime("2014-01-01 08:00:00"), "2000", 60, 0, 0), new Period(0, 15, 0, 0))

    computer.saveState(format.parseDateTime("2014-01-01 08:40:00"))

    StatAgentPeriodicTestUtils.allStatAgentPeriodic().map(_.copy(id=None)) shouldEqual List(
      StatAgentPeriodic(None, format.parseDateTime("2014-01-01 08:00:00"), "2000", 900, 840, 0),
      StatAgentPeriodic(None, format.parseDateTime("2014-01-01 08:15:00"), "2000", 900, 900, 0),
      StatAgentPeriodic(None, format.parseDateTime("2014-01-01 08:30:00"), "2000", 600, 600, 0)
    )
    AgentStateTestUtils.getStates() shouldEqual Map("2000" -> AgentState(format.parseDateTime("2014-01-01 08:40:00"), State.Paused))
    AgentStateTestUtils.countStates() shouldEqual 1
  }

  it should "return old periods with propagated state when notified of a new interval" in {
    computer.notifyNewInterval(format.parseDateTime("2014-01-01 09:03:45")) shouldEqual List(
      StatAgentPeriodic(None, format.parseDateTime("2014-01-01 08:00:00"), "2000", 900, 0, 0),
      StatAgentPeriodic(None, format.parseDateTime("2014-01-01 08:15:00"), "2000", 900, 0, 0),
      StatAgentPeriodic(None, format.parseDateTime("2014-01-01 08:30:00"), "2000", 900, 0, 0),
      StatAgentPeriodic(None, format.parseDateTime("2014-01-01 08:45:00"), "2000", 900, 0, 0)
    )
    computer.lastPeriod shouldEqual StatAgentPeriodic(None, format.parseDateTime("2014-01-01 09:00:00"), "2000", 225, 0, 0)
    computer.agentState shouldEqual AgentState(format.parseDateTime("2014-01-01 09:03:45"), State.LoggedOn)
  }

  it should "process a PAUSEALL queue log" in {
    computer.processQueueLog(QueueLog(8, format.parseDateTime("2014-01-01 08:01:02"), "NONE", "NONE", "2000", "PAUSEALL", None, None, None, None, None))
    computer.agentState shouldEqual AgentState(format.parseDateTime("2014-01-01 08:01:02"), State.Paused)
    computer.lastPeriod.loginTime shouldEqual 62
  }

  it should "process a UNPAUSEALL queue log" in {
    computer.agentState = computer.agentState.copy(state = State.Paused)
    computer.processQueueLog(QueueLog(8, format.parseDateTime("2014-01-01 08:01:02"), "NONE", "NONE", "2000", "UNPAUSEALL", None, None, None, None, None))
    computer.agentState shouldEqual AgentState(format.parseDateTime("2014-01-01 08:01:02"), State.LoggedOn)
    computer.lastPeriod.loginTime shouldEqual 62
    computer.lastPeriod.pauseTime shouldEqual 62
  }

  it should "process a AGENTCALLBACKLOGOFF queue log" in {
    computer.processQueueLog(QueueLog(8, format.parseDateTime("2014-01-01 08:01:02"), "NONE", "NONE", "2000", "AGENTCALLBACKLOGOFF", None, None, None, None, None))
    computer.agentState shouldEqual AgentState(format.parseDateTime("2014-01-01 08:01:02"), State.LoggedOff)
  }

  it should "process a AGENTCALLBACKLOGIN queue log" in {
    computer.agentState = computer.agentState.copy(state = State.LoggedOff)
    computer.processQueueLog(QueueLog(8, format.parseDateTime("2014-01-01 08:01:02"), "NONE", "NONE", "2000", "AGENTCALLBACKLOGIN", None, None, None, None, None))
    computer.agentState shouldEqual AgentState(format.parseDateTime("2014-01-01 08:01:02"), State.LoggedOn)
    computer.lastPeriod.loginTime shouldEqual 0
  }

  it should "process a WRAPUPSTART  queue log" in {
    computer.agentState = computer.agentState.copy(state = State.Paused)
    computer.processQueueLog(QueueLog(8, format.parseDateTime("2014-01-01 08:01:02"), "NONE", "NONE", "2000", "WRAPUPSTART", Some("15"), None, None, None, None))
    computer.agentState shouldEqual AgentState(format.parseDateTime("2014-01-01 08:01:02"), State.Paused)
    computer.lastPeriod.wrapupTime shouldEqual 15
    computer.lastPeriod.loginTime shouldEqual 62
    computer.lastPeriod.pauseTime shouldEqual 62
  }
}

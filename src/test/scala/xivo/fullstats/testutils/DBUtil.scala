package xivo.fullstats.testutils

import java.io.File
import java.sql.{Connection, DriverManager}
import java.util.Properties

import anorm.SQL
import org.dbunit.database.DatabaseConnection
import org.dbunit.dataset.DefaultDataSet
import org.dbunit.dataset.csv.{CsvDataSet, CsvDataSetWriter}
import org.dbunit.dataset.xml.{FlatXmlDataSet, FlatXmlProducer}
import org.dbunit.operation.DatabaseOperation
import org.xml.sax.InputSource

object DBUtil {

  val HOST = "localhost"
  val DB_NAME = "asterisktest"
  val USER = "asterisk"
  val PASSWORD = "asterisk"
  val connection = getConnection
  var setupDone = false

  def getConnection: Connection = {
    Class.forName("org.postgresql.Driver")
    val uri = "jdbc:postgresql://" + HOST + "/" + DB_NAME
    val props = new Properties()
    props.setProperty("user", USER)
    props.setProperty("password", PASSWORD)
    DriverManager.getConnection(uri, props)
  }

  def setupDB(filename: String)(implicit connection: Connection) = {
    if(!setupDone) {
      val dataset = new FlatXmlDataSet(new FlatXmlProducer(new InputSource(getClass.getClassLoader.getResourceAsStream(filename))))
      for (table <- dataset.getTableNames) {
        val createTable = scala.io.Source.fromInputStream(getClass.getClassLoader.getResourceAsStream(s"$table.sql")).mkString
        SQL(createTable).execute
      }
      val dbunitConnection: DatabaseConnection = new DatabaseConnection(connection)
      DatabaseOperation.CLEAN_INSERT.execute(dbunitConnection, dataset)
      setupDone = true
    }
  }

  def cleanTables(tables: List[String])(implicit connection: Connection) {
    val st = connection.createStatement()
    st.executeUpdate(s"TRUNCATE ${tables.mkString(",")} CASCADE")
  }

  def createCsv(tableName: String) = {
    var table = new DatabaseConnection(connection).createTable(tableName)
    val dataset = new DefaultDataSet(table)
    CsvDataSetWriter.write(dataset, new File(getClass.getClassLoader.getResource("CSV/").getFile))
  }

  def insertFromCsv() = {
    val dataset = new CsvDataSet(new File(getClass.getClassLoader.getResource("CSV/").getFile))
    DatabaseOperation.CLEAN_INSERT.execute(new DatabaseConnection(connection), dataset)
  }

}
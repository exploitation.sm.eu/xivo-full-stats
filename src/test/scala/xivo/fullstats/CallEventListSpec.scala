package xivo.fullstats

import xivo.fullstats.model.{CelTestUtils, QueueLog, QueueLogTestUtils}
import xivo.fullstats.testutils.{DBTest, DBUtil}
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future


class CallEventListSpec extends DBTest(List("cel", "queue_log")) {

  "A CallEventList" should "retrieve and iterate over call events in the right order, until told to stop" in {
    val startQlId = 42
    val additionalQlCid = "13455.789"
    val ql0 = QueueLog(40, formatMs.parseDateTime("2013-01-01 07:59:00.123"), "13455.789", "queue1", "NONE",
      "ENTERQUEUE", None, None, None, None, None)
    val ql1 = QueueLog(41, formatMs.parseDateTime("2013-01-01 08:00:00.753"), "13456.789", "queue1", "NONE",
      "ENTERQUEUE", None, None, None, None, None)
    val ql2 = QueueLog(42, formatMs.parseDateTime("2013-01-01 08:10:00.852"), "13456.789", "queue1", "agent1",
      "CONNECT", None, None, None, None, None)
    val ql3 = QueueLog(43, formatMs.parseDateTime("2013-01-01 08:20:00.963"), "13456.790", "queue2", "NONE",
      "ENTERQUEUE", None, None, None, None, None)
    val ql4 = QueueLog(44, formatMs.parseDateTime("2013-01-01 08:30:00.147"), "13456.790", "queue2", "agent1",
      "CONNECT", None, None, None, None, None)
    val ql5 = QueueLog(45, formatMs.parseDateTime("2013-01-01 08:32:00.789"), "13455.789", "queue1", "NONE",
      "ABANDON", None, None, None, None, None)
    val ql6 = QueueLog(46, formatMs.parseDateTime("2013-01-01 08:34:00.456"), "13456.791", "queue2", "agent1",
      "AGENTCALLBACKLOGIN", None, None, None, None, None)
    List(ql0, ql3, ql2, ql4, ql1, ql5, ql6).foreach(QueueLogTestUtils.insertQueueLog)

    val startCelId = 12
    val additionalCelCid = "1392999905.102"
    val c1 = EmptyObjectProvider.emptyCel().copy(id=10, linkedId = "123456.788", uniqueId="123456.788", eventType = "LINKEDID_END",
      eventTime=formatMs.parseDateTime("2013-01-01 08:10:11.312"))
    val c2 = EmptyObjectProvider.emptyCel().copy(id=11, linkedId = additionalCelCid, uniqueId = additionalCelCid, eventType = "APP_START",
      eventTime=formatMs.parseDateTime("2013-01-01 08:13:11.312"))
    val c3 = EmptyObjectProvider.emptyCel().copy(id=12, linkedId = "123456.789", uniqueId="123456.789", eventType = "CHAN_START",
      eventTime=formatMs.parseDateTime("2013-01-01 08:21:11.312"))
    val c4 = EmptyObjectProvider.emptyCel().copy(id=13, linkedId = "123456.789", uniqueId="123456.789", eventType = "XIVO_INCALL",
      eventTime=formatMs.parseDateTime("2013-01-01 08:31:11.312"))
    val c5 = EmptyObjectProvider.emptyCel().copy(id=14, linkedId = "123456.789", uniqueId="123456.789", eventType = "CHAN_END",
      eventTime=formatMs.parseDateTime("2013-01-01 08:33:11.312"))
    List(c1, c2, c3, c4, c5).foreach(CelTestUtils.insertCel)

    val evtList = new CallEventList(startCelId, startQlId, List(additionalCelCid), List(additionalQlCid))(new ConnectionFactory(
      DBUtil.HOST, DBUtil.DB_NAME, DBUtil.USER, DBUtil.PASSWORD))

    val it = evtList.iterator
    it.next() shouldEqual ql0
    it.next() shouldEqual ql2
    it.next() shouldEqual c2
    it.next() shouldEqual ql3
    it.next() shouldEqual c3
    it.next() shouldEqual ql4
    it.next() shouldEqual c4
    it.next() shouldEqual ql5
    it.next() shouldEqual c5
    it.next() shouldEqual ql6
    evtList.close()
    it.hasNext shouldBe false
  }

  "CallEventIterator.hasNext" should "block until some data becomes available" in {
    val ql0 = QueueLog(40, formatMs.parseDateTime("2013-01-01 07:59:00.123"), "13455.789", "queue1", "NONE",
      "ENTERQUEUE", None, None, None, None, None)
    val ql1 = QueueLog(41, formatMs.parseDateTime("2013-01-01 08:11:00.753"), "13456.789", "queue1", "NONE",
      "ENTERQUEUE", None, None, None, None, None)
    QueueLogTestUtils.insertQueueLog(ql0)

    val c1 = EmptyObjectProvider.emptyCel().copy(id=10, linkedId = "123456.788", uniqueId="123456.788", eventType = "LINKEDID_END",
      eventTime=formatMs.parseDateTime("2013-01-01 08:10:11.312"))
    val c2 = EmptyObjectProvider.emptyCel().copy(id=11, linkedId = "123456.788", uniqueId = "123456.788", eventType = "APP_START",
      eventTime=formatMs.parseDateTime("2013-01-01 08:13:11.312"))
    CelTestUtils.insertCel(c1)

    val evtList = new CallEventList(0, 0, List(), List())(new ConnectionFactory(DBUtil.HOST, DBUtil.DB_NAME, DBUtil.USER, DBUtil.PASSWORD))

    val it = evtList.iterator
    it.hasNext shouldBe true
    it.next() shouldEqual ql0
    it.hasNext shouldBe true
    it.next() shouldEqual c1
    val f = Future({
      val t1 = System.currentTimeMillis()
      it.hasNext shouldBe true
      val t2 = System.currentTimeMillis()
      t2 - t1 >= 200 shouldBe true
      it.next() shouldEqual ql1
      it.hasNext shouldBe true
      it.next() shouldEqual c2
      it.hasNext shouldBe false
    })
    Thread.sleep(200)
    QueueLogTestUtils.insertQueueLog(ql1)
    CelTestUtils.insertCel(c2)
    Thread.sleep(200)
    evtList.close()
    Thread.sleep(200)
    f.isCompleted shouldBe true
  }
}

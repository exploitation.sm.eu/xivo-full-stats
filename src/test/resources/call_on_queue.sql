DROP TYPE IF EXISTS "call_exit_type" CASCADE;
CREATE TYPE "call_exit_type" AS ENUM (
  'full',
  'closed',
  'joinempty',
  'leaveempty',
  'divert_ca_ratio',
  'divert_waittime',
  'answered',
  'abandoned',
  'timeout',
  'exit_with_key'
);

DROP TABLE IF EXISTS "call_on_queue" CASCADE;
CREATE TABLE "call_on_queue" (
 "id" SERIAL PRIMARY KEY,
 "callid" VARCHAR(32),
 "queue_time" timestamp NOT NULL,
 "total_ring_seconds" INTEGER NOT NULL DEFAULT 0,
 "answer_time" timestamp,
 "hangup_time" timestamp ,
 "status" call_exit_type,
 "queue_ref" VARCHAR(128),
 "agent_num" VARCHAR(128)
);
CREATE INDEX "call_on_queue__idx__callid" ON call_on_queue(callid);
CREATE INDEX "call_on_queue__idx__queue_time" ON call_on_queue(queue_time);

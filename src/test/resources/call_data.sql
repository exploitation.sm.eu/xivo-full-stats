DROP TABLE IF EXISTS call_element CASCADE;
DROP TABLE IF EXISTS call_data CASCADE;

DROP TYPE IF EXISTS status_type CASCADE;
CREATE TYPE status_type AS ENUM ('answer', 'busy', 'cancel', 'failed');

DROP TYPE IF EXISTS call_direction_type CASCADE;
CREATE TYPE call_direction_type AS ENUM('incoming', 'outgoing', 'internal');

CREATE TABLE call_data (
    id SERIAL PRIMARY KEY,
    uniqueid VARCHAR(150) NOT NULL,
    dst_num VARCHAR(80),
    start_time TIMESTAMP WITHOUT TIME ZONE,
    answer_time TIMESTAMP WITHOUT TIME ZONE,
    end_time TIMESTAMP WITHOUT TIME ZONE,
    status status_type,
    ring_duration_on_answer INTEGER,
    transfered BOOLEAN,
    call_direction call_direction_type,
    src_num VARCHAR(80),
    transfer_direction call_direction_type,
    src_agent VARCHAR(128),
    dst_agent VARCHAR(128),
    src_interface VARCHAR(255)
);

CREATE INDEX call_data__idx__end_time ON call_data(end_time);
CREATE INDEX call_data__idx__start_time ON call_data(start_time);

CREATE TABLE call_element (
    id SERIAL PRIMARY KEY,
    call_data_id INTEGER REFERENCES call_data(id) NOT NULL,
    start_time TIMESTAMP WITHOUT TIME ZONE,
    answer_time TIMESTAMP WITHOUT TIME ZONE,
    end_time TIMESTAMP WITHOUT TIME ZONE,
    interface VARCHAR(255),
    agent VARCHAR(128)
);
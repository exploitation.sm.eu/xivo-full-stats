DROP TABLE IF EXISTS agent_position CASCADE;

CREATE TABLE agent_position (
    agent_num VARCHAR(50) NOT NULL,
    line_number VARCHAR(10),
    start_time TIMESTAMP WITHOUT TIME ZONE,
    end_time TIMESTAMP WITHOUT TIME ZONE,
    sda VARCHAR(40)
);

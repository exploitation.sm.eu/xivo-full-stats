package xivo.fullstats

import java.sql.{Connection, DriverManager}
import java.util.Properties

import org.slf4j.LoggerFactory

class ConnectionFactory(host: String, dbName: String, dbUser: String, dbPassword: String) {
  val logger = LoggerFactory.getLogger(getClass)
  logger.info("----- Database configuration -------")
  logger.info(s"host    : $host")
  logger.info(s"db name : $dbName")
  logger.info(s"db user : $dbUser")

  def getConnection: Connection = {
    Class.forName("org.postgresql.Driver")
    val uri = "jdbc:postgresql://" + host + "/" + dbName
    val props = new Properties()
    props.setProperty("user", dbUser)
    props.setProperty("password", dbPassword)
    props.setProperty("stringtype", "unspecified")
    DriverManager.getConnection(uri, props)
  }

}
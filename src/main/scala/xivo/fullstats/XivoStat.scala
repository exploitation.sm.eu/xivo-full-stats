package xivo.fullstats

import org.joda.time.Period
import org.slf4j.LoggerFactory
import xivo.fullstats.agent.{AgentPositionManager, StatAgentManager}
import xivo.fullstats.callparsers.{CallDataGenerator, CallOnQueueGenerator}
import xivo.fullstats.ids.{CelIdManager, QueueLogIdManager}
import xivo.fullstats.model._
import xivo.fullstats.queue.StatQueueManager

class XivoStat(factory: ConnectionFactory, interval: Period) {
  implicit val connection = factory.getConnection
  val logger = LoggerFactory.getLogger(getClass)
  var eventList: CallEventList = null
  var terminated = false

  def calculateStats() {
    CallOnQueue.deleteStalePendingCalls()
    val pendingQlCids = CallOnQueue.getPendingCallIds()
    logger.info(s"Removing pending calls for table call_on_queue : $pendingQlCids")
    CallOnQueue.deleteByCallid(pendingQlCids)

    CallData.deleteStalePendingCalls()
    val pendingCelCids = CallData.getPendingCallIds()
    logger.info(s"Removing pending calls for table call_data : $pendingCelCids")
    CallData.deleteByCallid(pendingCelCids)

    eventList = new CallEventList(Cel.getLastId() + 1, QueueLog.getLastId() + 1, pendingCelCids, pendingQlCids)(factory)
    new CallEventsLoop(
      eventList,
      List(
        new CallDataGenerator,
        new CelIdManager),
      List(
        new CallOnQueueGenerator,
        new AgentPositionManager(),
        new StatAgentManager(interval),
        new StatQueueManager(interval, QueueLog.getLastId() + 1),
        new QueueLogIdManager)
    ).processNewCallEvents()
    terminated = true
  }

  def terminate(): Unit = {
    val maxRetries = 10
    var nbRetries = 0
    while(eventList == null && nbRetries < maxRetries) {
      logger.info("Waiting 1 second for eventList to be created before stopping...")
      Thread.sleep(1000)
      nbRetries += 1
    }
    if(eventList != null) {
      logger.info("Asking call events processing to terminate...")
      eventList.close()
      nbRetries = 0
      while(!terminated && nbRetries < maxRetries) {
        logger.info("Waiting 1 second for current state to be saved...")
        Thread.sleep(1000)
        nbRetries += 1
      }
      if(!terminated) {
        logger.warn("Could not terminate safely, exiting !")
      }
    } else {
      logger.warn("EventList was not created, impossible to stop !")
    }
  }
}

package xivo.fullstats

import java.sql.{Connection, PreparedStatement, ResultSet}

import org.joda.time.DateTime
import xivo.fullstats.model.{CallEvent, Cel, QueueLog}

class CallEventList(startCelId: Int, startQueueLogId: Int, additionalCelCids: List[String], additionalQueueLogCids: List[String])(implicit factory: ConnectionFactory)
  extends Iterable[CallEvent] {

  @volatile var isClosed = false
  override def iterator: Iterator[CallEvent] = new CallEventIterator(startCelId, startQueueLogId, additionalCelCids, additionalQueueLogCids, this)

  def close(): Unit = isClosed = true
}

class CallEventIterator(var startCelId: Int, var startQueueLogId: Int, additionalCelCids: List[String], additionalQueueLogCids: List[String], evtList: CallEventList)
                       (implicit factory: ConnectionFactory) extends Iterator[CallEvent] {

  var connection = factory.getConnection
  connection.setAutoCommit(false)
  var celIterator = new CelIterator(startCelId, additionalCelCids, connection)
  var queueLogIterator = new QueueLogIterator(startQueueLogId, additionalQueueLogCids, connection)
  
  var nextCel: Option[Cel] = None
  var nextQueueLog: Option[QueueLog] = None

  setNextCel()
  setNextQueueLog()
  
  def setNextCel(): Unit = {
    nextCel = if(celIterator.hasNext) Some(celIterator.next()) else None
    startCelId = nextCel.map(_.id + 1).getOrElse(startCelId)
  }
  
  def setNextQueueLog(): Unit = {
    nextQueueLog = if(queueLogIterator.hasNext) Some(queueLogIterator.next()) else None
    startQueueLogId = nextQueueLog.map(_.id + 1).getOrElse(startQueueLogId)
  }

  override def hasNext: Boolean = {
    if(evtList.isClosed) return false
    while(nextQueueLog.isEmpty && nextCel.isEmpty) {
      if(evtList.isClosed) return false
      Thread.sleep(100)
      searchNewData()
    }
    true
  }

  private def searchNewData(): Unit = {
    resetIterators()
    setNextQueueLog()
    setNextCel()
  }

  private def resetIterators(): Unit = {
    connection.close()
    connection = factory.getConnection
    connection.setAutoCommit(false)
    celIterator = new CelIterator(startCelId, List(), connection)
    queueLogIterator = new QueueLogIterator(startQueueLogId, List(), connection)
  }

  override def next(): CallEvent = {
    var res: Option[CallEvent] = None
    if(nextQueueLog.isDefined) {
      if(nextCel.isDefined && nextQueueLog.get.time.isAfter(nextCel.get.eventTime)) {
        res = nextCel
        setNextCel()
      } else {
        res = nextQueueLog
        setNextQueueLog()
      }
    } else {
      res = nextCel
      setNextCel()
    }
    res.get
  }
}

abstract class GenericCallEventIterator[E <: CallEvent](val connection: Connection) extends Iterator[E] {
  val batchSize = 50000

  val rs: ResultSet = {
    val st = createStatement()
    st.setFetchSize(batchSize)
    st.executeQuery()
  }

  def createStatement(): PreparedStatement

  override def hasNext: Boolean = {
    val res = rs.next()
    if(!res)
      connection.close()
    res
  }
}

class CelIterator(startId: Int, additionalCids: List[String], override val connection: Connection) extends GenericCallEventIterator[Cel](connection) {

  private  def requestAdditionnalCids(cids: List[String]): String = {
    var idVars = ""
    for (i <- 1 to cids.size)
      idVars += s"?,"
    if(idVars.length > 0)
      idVars = idVars.substring(0, idVars.length - 1)
    s"select * from cel where  linkedid in ( $idVars )"
  }

  override def next(): Cel = Cel(
    rs.getInt("id"),
    rs.getString("eventtype"),
    new DateTime(rs.getTimestamp("eventtime")),
    rs.getString("userdeftype"),
    rs.getString("cid_name"),
    rs.getString("cid_num"),
    rs.getString("cid_ani"),
    rs.getString("cid_rdnis"),
    rs.getString("cid_dnid"),
    rs.getString("exten"),
    rs.getString("context"),
    rs.getString("channame"),
    rs.getString("appname"),
    rs.getString("appdata"),
    rs.getInt("amaflags"),
    rs.getString("accountcode"),
    rs.getString("peeraccount"),
    rs.getString("uniqueid"),
    rs.getString("linkedid"),
    rs.getString("userfield"),
    rs.getString("peer"),
    None)

  override def createStatement(): PreparedStatement = {
    var query = s"select * from cel where id >= $startId order by id asc"
    if(additionalCids.size > 0) {
      query = s"(($query) UNION ${requestAdditionnalCids(additionalCids)}) order by id asc"
    }
    val statement = connection.prepareStatement(query)
    for(i <- 1 to additionalCids.size)
      statement.setString(i, additionalCids(i-1))
    statement
  }
}

class QueueLogIterator(startId: Int, additionalCids: List[String], override val connection: Connection) extends GenericCallEventIterator[QueueLog](connection) {

  private  def requestAdditionnalCids(cids: List[String]): String = {
    var idVars = ""
    for (i <- 1 to cids.size)
      idVars += s"?,"
    if(idVars.length > 0)
      idVars = idVars.substring(0, idVars.length - 1)
    s"callid IN ( $idVars )"
  }

  override def next(): QueueLog = QueueLog(
    rs.getInt("id"),
    new DateTime(rs.getTimestamp("time")),
    rs.getString("callid"),
    rs.getString("queuename"),
    rs.getString("agent"),
    rs.getString("event"),
    Option(rs.getString("data1")),
    Option(rs.getString("data2")),
    Option(rs.getString("data3")),
    Option(rs.getString("data4")),
    Option(rs.getString("data5")))

  override def createStatement(): PreparedStatement = {
    var query = s"SELECT id, to_timestamp(time, 'YYYY-MM-DD HH24:MI:SS.US') AS time, callid, queuename, agent, event, data1, data2, data3, data4, data5 FROM queue_log WHERE id >= $startId"
    if(additionalCids.size > 0)
      query += s" OR ${requestAdditionnalCids(additionalCids)}"
    query +=  " ORDER BY id ASC"

    val statement = connection.prepareStatement(query)
    for(i <- 1 to additionalCids.size)
      statement.setString(i, additionalCids(i-1))
    statement
  }
}

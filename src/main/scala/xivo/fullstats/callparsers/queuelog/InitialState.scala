package xivo.fullstats.callparsers.queuelog

import java.sql.Connection

import xivo.fullstats.model.{CallExitType, CallOnQueue, Cel, QueueLog}

class InitialState(call: CallOnQueue)(implicit c: Connection) extends QueueLogState(call, false) {

  override def processEvent(ql: QueueLog): QueueLogState = {
    result.queueTime = ql.time
    result.queueRef = ql.queueName
    result.callid = Cel.linkedIdFromUniqueId(ql.callid) match {
      case None => logger.warn(s"Could not find a CEL with uniqueid ${ql.callid} during initial state")
        None
      case Some(linkedId) => Some(linkedId)
    }

    ql.event match {
      case "ENTERQUEUE" => new Queued(result)

      case "CLOSED" => result.status = Some(CallExitType.Closed)
        result.hangupTime = Some(ql.time)
        new Finished(result, ql)

      case "FULL" => result.status = Some(CallExitType.Full)
        result.hangupTime = Some(ql.time)
        new Finished(result, ql)

      case "DIVERT_CA_RATIO" => result.status = Some(CallExitType.DivertCaRatio)
        result.hangupTime = Some(ql.time)
        new Finished(result, ql)

      case "DIVERT_HOLDTIME" => result.status = Some(CallExitType.DivertWaitTime)
        result.hangupTime = Some(ql.time)
        new Finished(result, ql)

      case "JOINEMPTY" => result.status = Some(CallExitType.JoinEmpty)
        result.hangupTime = Some(ql.time)
        new Finished(result, ql)

      case _ => processUnexpectedQueueLog(ql)
    }

  }
}

package xivo.fullstats.callparsers.queuelog

import java.sql.Connection

import xivo.fullstats.model.{CallOnQueue, QueueLog}

class Answered(call: CallOnQueue)(implicit c: Connection) extends QueueLogState(call, false) {
  override def processEvent(ql: QueueLog): QueueLogState = ql.event match {
    case "COMPLETEAGENT" | "COMPLETECALLER" => result.hangupTime = Some(ql.time)
      new Finished(result, ql)

    case "TRANSFER" => result.hangupTime = Some(ql.time)
      new Finished(result, ql)

    case _ => processUnexpectedQueueLog(ql)
  }
}

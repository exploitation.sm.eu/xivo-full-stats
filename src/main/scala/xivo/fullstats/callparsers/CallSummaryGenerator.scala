package xivo.fullstats.callparsers

import java.sql.Connection

import org.joda.time.DateTime
import org.slf4j.LoggerFactory
import xivo.fullstats.EventParser
import xivo.fullstats.agent.AgentPositionProxy
import xivo.fullstats.model._
import xivo.fullstats.callparsers.cel.CelMachine
import xivo.fullstats.callparsers.queuelog.QueueLogMachine

import scala.collection.mutable

abstract class CallSummaryGenerator[E <: CallEvent, R <: CallSummary](stateMachineFactory: () => StateMachine[E, R],
                                                                      postProcessing: Option[R => Unit]) extends EventParser[E]{
  val logger = LoggerFactory.getLogger(getClass)
  val callMap = mutable.Map[String, StateMachine[E, R]]()
  var lastPurgeDate: Option[DateTime] = None

  def parseEvent(event: E): Unit = {
    callMap.get(event.callid) match {
      case None => logger.info(s"Processing new call on event $event")
        callMap.put(event.callid, stateMachineFactory())
      case _ =>
    }
    val machine = callMap(event.callid)
    logger.debug(s"Processing event $event")
    machine.processEvent(event)

    if(machine.isCallFinished) {
      val res = machine.getResult
      doPostProcessing(res)
      logger.info(s"The call $res finished with event $event")
      saveResult(res)
      callMap.remove(event.callid)
    }
    purgeIfRequired(event)
  }

  private def doPostProcessing(list: Iterable[R]): Unit = {
    postProcessing match {
      case None =>
      case Some(f) => list.foreach(f)
    }
  }

  def saveResult(res: Iterable[R])

  def saveState(): Unit = {
    val res = callMap.values.flatMap(_.getResult)
    doPostProcessing(res)
    logger.info(s"Saving the ongoing calls : $res")
    saveResult(res)
  }

  def purgeIfRequired(event: E): Unit = {
    if(lastPurgeDate.isEmpty) {
      lastPurgeDate = Some(event.time)
    }
    if(event.time.isAfter(lastPurgeDate.get.plusDays(1))) {
      doPurge(lastPurgeDate.get)
      lastPurgeDate = Some(event.time)
    }
  }

  def doPurge(time: DateTime): Unit = {
    for((callid, machine) <- callMap) {
      machine.getResult match {
        case Some(call) if call.getStartTime.isBefore(time) =>  callMap.remove(callid)
          machine.forceCloture()
          saveResult(machine.getResult)
        case _ =>
      }
    }
  }
}

class CallDataGenerator(implicit c: Connection) extends CallSummaryGenerator[Cel, CallData](() => new CelMachine, Some(cd => {
  if(cd.srcNum.isDefined)
    cd.srcAgent = AgentPositionProxy.agentForNumAndTime(cd.srcNum.get, cd.startTime)
  if(cd.dstNum.isDefined)
    cd.dstAgent = AgentPositionProxy.agentForNumAndTime(cd.dstNum.get, cd.startTime)
})) {
  override def saveResult(res: Iterable[CallData]): Unit = res.foreach(CallData.insertOrUpdate)
}

class CallOnQueueGenerator(implicit c: Connection) extends CallSummaryGenerator[QueueLog, CallOnQueue](() => new QueueLogMachine(), None) {
  override def saveResult(res: Iterable[CallOnQueue]): Unit = res.foreach(CallOnQueue.insertOrUpdate)

  val CallEvents = List("ENTERQUEUE", "CONNECT", "COMPLETEAGENT", "COMPLETECALLER", "TRANSFER", "ABANDON", "EXITWITHKEY",
    "EXITWITHTIMEOUT", "CLOSED", "DIVERT_CA_RATIO", "DIVERT_HOLDTIME", "FULL", "LEAVEEMPTY", "JOINEMPTY", "RINGNOANSWER")

  override def parseEvent(ql: QueueLog): Unit = {
    if(CallEvents.contains(ql.event))
      super.parseEvent(ql)
  }
}
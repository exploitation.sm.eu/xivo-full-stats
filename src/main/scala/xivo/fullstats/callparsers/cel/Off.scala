package xivo.fullstats.callparsers.cel

import org.joda.time.DateTime
import xivo.fullstats.model.{Cel, CallData}

class Off(var callData: CallData) extends CelState(callData, false) {

  override def processCel(cel: Cel): CelState = {
    if (cel.eventType.equals("CHAN_START")) {
      if (cel.exten != "s")
        result.dstNum = Some(cel.exten)
      result.uniqueId = cel.linkedId
      result.startTime = new DateTime(cel.eventTime)
      result.srcNum = Some(cel.cidNum)
      result.srcInterface = extractInterface(cel.chanName)
      return new Started(result, cel)
    }
    throw new InvalidCelException
  }

  private def extractInterface(channName: String): Option[String] = {
    val splitRes = channName.reverse.split("-", 2)
    if(splitRes.size != 2)
      None
    else
      Some(splitRes(1).reverse)
  }

}
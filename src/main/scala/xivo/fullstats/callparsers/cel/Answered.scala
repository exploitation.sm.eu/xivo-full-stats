package xivo.fullstats.callparsers.cel

import org.joda.time.DateTime
import xivo.fullstats.model.{CallDirection, HoldPeriod, Cel, CallData}

class Answered(callData: CallData, cel: Cel) extends CelState(callData, false) {
  result.status = Some("answer")
  result.answerTime = Some(new DateTime(cel.eventTime))
  
  override def processCel(cel: Cel): CelState = {
    if(cel.eventType.equals("LINKEDID_END")) {
      return new HangedUp(result, cel)
    } else if(cel.eventType.equals("HOLD")) {
      result.addHoldPeriod(HoldPeriod(new DateTime(cel.eventTime), None, cel.linkedId))
    }else if(cel.eventType.equals("UNHOLD")) {
      result.holdPeriods.headOption.foreach(p => p.end = Some(new DateTime(cel.eventTime)))
    } else if(cel.eventType.equals("BLINDTRANSFER")) {
      processTransfer()
    } else if(cel.chanName.contains("ZOMBIE") && cel.uniqueId.equals(result.uniqueId)) {
      processTransfer()
    } else if(result.transfered && cel.eventType.equals("XIVO_OUTCALL")) {
      result.transferDirection = Some(CallDirection.Outgoing)
    }
    this
  }

  private def processTransfer() {
    result.transfered = true
    result.transferDirection = Some(CallDirection.Internal)
  }
}
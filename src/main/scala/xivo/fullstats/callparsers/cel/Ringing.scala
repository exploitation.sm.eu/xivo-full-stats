package xivo.fullstats.callparsers.cel

import xivo.fullstats.model.{CallData, Cel}

class Ringing(callData: CallData, cel: Cel) extends CelState(callData, false) {
  var ringStartTime = cel.eventTime
    
  override def processCel(cel: Cel): CelState = {
    if(cel.eventType.equals("ANSWER") && cel.appName.equals("AppDial")) {
      val seconds = (cel.eventTime.toDate.getTime - ringStartTime.toDate.getTime)/1000
      result.ringDurationOnAnswer = Some(seconds.toInt)
      if(result.dstNum.isEmpty)
        result.dstNum = Some(cel.cidNum)
      return new Answered(result, cel)
    } else if(cel.eventType.equals("HANGUP") && cel.appName.equals("AppDial")) {
      return new Started(result, cel)
    } else if(cel.eventType.equals("LINKEDID_END")) {
      return new HangedUp(result, cel)
    }
    this
  }
}
package xivo.fullstats.callparsers.cel

import java.sql.Connection

import org.slf4j.LoggerFactory
import xivo.fullstats.model.Cel

import scala.collection.mutable.ListBuffer

class ChannelManager(val callDataId: Int)(implicit c: Connection) {
  val logger = LoggerFactory.getLogger(getClass)
  var channels = ListBuffer[ChannelMachine]()

  def processEvent(cel: Cel): Unit = {
    (cel.eventType, cel.appName) match {
      case ("APP_START", "Dial") => if(cel.appData.nonEmpty) cel.appData.split(",").head.split("&").foreach(interface =>
        channels.append(new ChannelMachine(interface, callDataId, cel.eventTime)))
      case ("BRIDGE_UPDATE", "Dial") => if(cel.appData.nonEmpty) cel.appData.split(",").head.split("&").foreach(interface =>
        channels.append(new ChannelMachine(interface, callDataId, cel.eventTime, true)))
      case ("LINKEDID_END", _) => channels.foreach(_.processEvent(cel))
      case _ => channels.find(_.isEventForMe(cel)).foreach(machine => {
        machine.processEvent(cel)
        if(machine.isChannelHangedUp) channels -= machine
      })
    }
  }

  def forceCloture(): Unit = channels.foreach(_.forceCloture())
}

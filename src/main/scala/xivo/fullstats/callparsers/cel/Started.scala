package xivo.fullstats.callparsers.cel

import xivo.fullstats.model.{CallDirection, Cel, CallData}

class Started(callData: CallData, cel: Cel) extends CelState(callData, false) {

  override def processCel(cel: Cel): CelState = {
    if (cel.eventType.equals("ANSWER")) {
      if (result.dstNum.isEmpty)
        result.dstNum = Some(cel.cidNum)
      if(cel.appName.equals("AppDial"))
        return new Answered(result, cel)
    } else if (cel.eventType.equals("LINKEDID_END")) {
      return new HangedUp(result, cel)
    } else if (cel.eventType.equals("APP_START") && cel.appName.equals("Dial")) {
      return new Ringing(result, cel)
    } else if (cel.eventType.equals("XIVO_OUTCALL")) {
      result.callDirection = CallDirection.Outgoing
    } else if (cel.eventType.equals("XIVO_INCALL")) {
      result.callDirection = CallDirection.Incoming
    } else if(cel.eventType.equals("OUTCALL_ACD")) {
      result.dstNum = Some(cel.cidNum)
      return new StartedAcd(result, cel)
    }
    this
  }
}

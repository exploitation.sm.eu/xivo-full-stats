package xivo.fullstats.callparsers.cel

import java.sql.Connection

import org.joda.time.DateTime
import xivo.fullstats.model.{CallElement, Cel}

class ChannelMachine(val interface: String, callDataId: Long, startTime: DateTime, answered: Boolean = false)(implicit c: Connection)  {
  var callElement = CallElement.insert(CallElement(None, callDataId, startTime, if(answered) Some(startTime) else None, None, interface, None))
  var channelHangedUp = false

  def isChannelHangedUp = channelHangedUp

  def processEvent(cel: Cel): Unit = (cel.eventType, cel.appName) match {
    case ("ANSWER", "AppDial") => callElement = callElement.copy(answerTime = Some(cel.eventTime))
      CallElement.update(callElement)
    case ("CHAN_END", _) | ("LINKEDID_END", _) => callElement = callElement.copy(endTime = Some(cel.eventTime))
      CallElement.update(callElement)
      channelHangedUp = true
    case _ =>
  }

  def isEventForMe(cel: Cel): Boolean = cel.chanName.startsWith(interface)

  def forceCloture(): Unit = if(callElement.endTime.isEmpty) {
    callElement = callElement.copy(endTime = Some(callElement.answerTime.getOrElse(callElement.startTime)))
    CallElement.update(callElement)
  }
}

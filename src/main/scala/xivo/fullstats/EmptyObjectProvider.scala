package xivo.fullstats

import org.joda.time.DateTime
import xivo.fullstats.model.{CallData, CallDirection, CallOnQueue, Cel}

object EmptyObjectProvider {
  def emptyCel(): Cel =
    Cel(0, null, DateTime.now(), "", "", "", "", "", "", "", "", "", "", "", 0, "", "", "", "", "", "", None)

  def emptyCallData(): CallData = CallData(None, null, None, CallDirection.Internal, DateTime.now(), None, None, None, None, false, None, None)

  def emptyCallOnQueue() = CallOnQueue(None, None, DateTime.now(), 0, None, None, None, "", None)
}
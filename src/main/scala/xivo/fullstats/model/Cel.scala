package xivo.fullstats.model

import java.sql.Connection

import anorm.SqlParser._
import anorm._
import org.joda.time.DateTime

case class Cel(
  override val id: Int,
  var eventType: String,
  var eventTime: DateTime,
  var userDefType: String,
  var cidName: String,
  var cidNum: String,
  var cidAni: String,
  var cidRdnis: String,
  var cidDnid: String,
  var exten: String,
  var context: String,
  var chanName: String,
  var appName: String,
  var appData: String,
  var amaFlags: Int,
  var accountCode: String,
  var peerAccount: String,
  var uniqueId: String,
  var linkedId: String,
  var userField: String,
  var peer: String,
  var callLogId: Option[Int]) extends CallEvent(id, linkedId, eventTime)

object Cel extends LastIdTable {
  override val lastIdTable: String = "last_cel_id"

  def linkedIdFromUniqueId(uniqueid: String)(implicit c: Connection): Option[String] = SQL("SELECT linkedid FROM cel WHERE uniqueid = {uniqueid} LIMIT 1")
    .on('uniqueid -> uniqueid).as(get[String]("linkedid") *).headOption
}

package xivo.fullstats.model

import java.sql.Connection

import org.joda.time.DateTime
import org.slf4j.LoggerFactory

trait CallSummary {
  def getStartTime: DateTime
}

trait CallSummaryTable[T <: CallSummary] {
  val logger = LoggerFactory.getLogger(getClass)

  def insertOrUpdate(call: T)(implicit c: Connection): T
}

package xivo.fullstats.model

import org.joda.time.DateTime

case class HoldPeriod(var start: DateTime, var end: Option[DateTime], linkedId: String)

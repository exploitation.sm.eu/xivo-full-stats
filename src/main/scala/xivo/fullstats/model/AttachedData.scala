package xivo.fullstats.model

case class AttachedData (key: String, value: String)
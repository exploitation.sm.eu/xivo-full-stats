package xivo.fullstats.model

import java.sql.Connection
import java.util.Date

import anorm.SqlParser._
import anorm.{SQL, ~}
import org.joda.time.DateTime

case class AgentState(date: DateTime, state: State.State)


object State extends Enumeration {
  type State = Value
  val LoggedOff = Value("logged_off")
  val LoggedOn = Value("logged_on")
  val Paused = Value("paused")
}

object AgentState {
  def lastForAgent(agentNum: String)(implicit c: Connection): Option[AgentState] = SQL("SELECT time, state::varchar FROM agent_states WHERE agent = {num} ORDER BY time DESC LIMIT 1")
    .on('num -> agentNum).as((get[Date]("time") ~ get[String]("state") map {
    case time ~ state => AgentState(new DateTime(time), State.withName(state))
  }) *).headOption

  def replaceState(agent: String, state: AgentState)(implicit c: Connection): Unit = {
    SQL("DELETE FROM agent_states WHERE agent = {agent}").on('agent -> agent).executeUpdate()
    SQL("INSERT INTO agent_states(agent, time, state) VALUES ({agent}, {time}, {state}::agent_state_type)")
      .on('agent -> agent, 'time -> state.date.toDate, 'state -> state.state.toString).executeUpdate()
  }
}
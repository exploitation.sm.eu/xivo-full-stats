package xivo.fullstats.model

import java.sql.Connection
import java.util.Date

import anorm.SqlParser._
import anorm._
import org.joda.time.DateTime

case class StatAgentPeriodic(id: Option[Int], time: DateTime, agent: String, var loginTime: Int, var pauseTime: Int, var wrapupTime: Int) extends Ordered[StatAgentPeriodic] {
  override def compare(that: StatAgentPeriodic): Int = this.toString compare that.toString
  def isEmpty: Boolean = loginTime == 0 && pauseTime == 0 && wrapupTime == 0
}

object StatAgentPeriodic {

  val period = get[Option[Int]]("id") ~
    get[Date]("time") ~
    get[String]("agent") ~
    get[Double]("login_time") ~
    get[Double]("pause_time") ~
    get[Double]("wrapup_time") map {
    case id ~ time ~ agent ~ login ~ pause ~ wrapup => StatAgentPeriodic(id, new DateTime(time), agent, login.toInt, pause.toInt, wrapup.toInt)
  }

  def lastForAgent(agentNum: String)(implicit c: Connection): Option[StatAgentPeriodic] =
    SQL("SELECT id, time, agent, extract(epoch FROM login_time) AS login_time, extract(epoch FROM pause_time) AS pause_time, extract(epoch FROM wrapup_time) AS wrapup_time " +
      "FROM stat_agent_periodic WHERE agent = {num} ORDER BY time DESC LIMIT 1")
    .on('num -> agentNum).as(period *).headOption

  def insertAll(stats: Iterable[StatAgentPeriodic])(implicit conn: Connection) {
    conn.setAutoCommit(false)
    stats.foreach(s => insertOrUpdate(s))
    conn.commit()
    conn.setAutoCommit(true)
  }

  private def insertOrUpdate(stat: StatAgentPeriodic)(implicit c: Connection): Unit = {
    if(stat.id.isDefined) update(stat)
    else insert(stat)
  }

  private def update(stat: StatAgentPeriodic)(implicit c: Connection): Unit = {
    SQL("UPDATE stat_agent_periodic SET login_time = {login} * INTERVAL '1 second', pause_time = {pause} * INTERVAL '1 second', " +
      "wrapup_time = {wrapup} * INTERVAL '1 second' WHERE id = {id}").on('id -> stat.id, 'login -> stat.loginTime, 'pause -> stat.pauseTime,
      'wrapup -> stat.wrapupTime).executeUpdate()
  }

  private def insert(stat: StatAgentPeriodic)(implicit conn: Connection) =
    SQL("""INSERT INTO stat_agent_periodic(time, agent, login_time, pause_time, wrapup_time) VALUES
      ({time}, {agent}, {loginTime} * INTERVAL '1 second', {pauseTime} * INTERVAL '1 second', {wrapupTime} * INTERVAL '1 second')""")
      .on('time -> stat.time.toDate, 'agent -> stat.agent, 'loginTime -> stat.loginTime, 'pauseTime -> stat.pauseTime, 'wrapupTime -> stat.wrapupTime)
      .executeUpdate()



}
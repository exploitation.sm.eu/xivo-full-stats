package xivo.fullstats.model

import java.sql.Connection
import java.util.Date

import anorm.SqlParser._
import anorm._
import org.joda.time.DateTime
import xivo.fullstats.model.CallDirection.CallDirection

object CallDirection extends Enumeration {
  type CallDirection = Value
  val Incoming = Value("incoming")
  val Outgoing = Value("outgoing")
  val Internal = Value("internal")
}

case class CallData(
  var id: Option[Int],
  var uniqueId: String,
  var dstNum: Option[String],
  var callDirection: CallDirection,
  var startTime: DateTime,
  var answerTime: Option[DateTime],
  var endTime: Option[DateTime],
  var status: Option[String],
  var ringDurationOnAnswer: Option[Int],
  var transfered: Boolean,
  var srcNum: Option[String],
  var transferDirection: Option[CallDirection],
  var srcAgent: Option[String] = None,
  var dstAgent: Option[String] = None,
  var srcInterface: Option[String] = None,
  var attachedData: List[AttachedData] = List(),
  var holdPeriods: List[HoldPeriod] = List()) extends CallSummary {

  def addAttachedData(data: AttachedData) = attachedData = attachedData ++ List(data)

  def addHoldPeriod(p: HoldPeriod) = holdPeriods = holdPeriods.+:(p)

  override def getStartTime: DateTime = startTime
}

object CallData extends CallSummaryTable[CallData] {

  val callDataInsertStmt = SQL("""INSERT INTO call_data(uniqueid, dst_num, call_direction, start_time, answer_time, end_time, status, ring_duration_on_answer, transfered, src_num, transfer_direction, src_agent, dst_agent, src_interface)
                VALUES ({uniqueid}, {dst_num}, {call_direction}::call_direction_type, {start_time}, {answer_time}, {end_time}, {status}::status_type,
                {ring_duration_on_answer}, {transfered}, {src_num}, {transfer_direction}::call_direction_type, {src_agent}, {dst_agent}, {src_interface})""")
  val callDataUpdateStmt = SQL("""UPDATE call_data SET uniqueid={uniqueid}, dst_num={dst_num}, call_direction={call_direction}::call_direction_type, start_time={start_time}, answer_time={answer_time}, end_time={end_time},
      status={status}::status_type, ring_duration_on_answer={ring_duration_on_answer}, transfered={transfered}, src_num={src_num}, transfer_direction={transfer_direction}::call_direction_type,
      src_agent={src_agent}, dst_agent={dst_agent}, src_interface={src_interface} WHERE id={id}""")
  val attachedDataInsertStmt = SQL("INSERT INTO attached_data (id_call_data, key, value) VALUES ({id_call_data}, {key}, {value})")
  val holdPeriodsInsertStmt = SQL("""INSERT INTO hold_periods(linkedid, start, "end") VALUES ({linkedid}, {start}, {end})""")


  def deleteStalePendingCalls()(implicit c: Connection) = {
    SQL("SELECT max(start_time) as max FROM call_data").as(get[Option[Date]]("max") *).head match {
      case None =>
      case Some(date) => deleteByCallid(SQL("SELECT uniqueid FROM call_data WHERE end_time IS NULL AND start_time < {maxDate}::timestamp - INTERVAL '1 day'")
        .on('maxDate -> date).as(get[String]("uniqueid") *))
    }
  }

  def getPendingCallIds()(implicit conn: Connection): List[String] = SQL("SELECT uniqueid FROM call_data WHERE end_time IS NULL").as(get[String]("uniqueid") *)

  def deleteByCallid(callids: List[String])(implicit c: Connection): Unit = {
    if(callids.size == 0)
      return
    c.setAutoCommit(false)
    try {
      val callDataInClause = SqlUtils.createInClauseOrFalse("uniqueid", callids)
      val holdPeriodInClause = SqlUtils.createInClauseOrFalse("linkedid", callids)
      SQL(s"DELETE FROM attached_data WHERE id_call_data IN (SELECT id FROM call_data WHERE $callDataInClause)").executeUpdate()
      SQL(s"DELETE FROM hold_periods WHERE $holdPeriodInClause").executeUpdate()
      SQL(s"DELETE FROM call_element WHERE call_data_id IN (SELECT id FROM call_data WHERE $callDataInClause)").executeUpdate()
      SQL(s"DELETE FROM call_data WHERE $callDataInClause").executeUpdate()
      c.commit()
    } catch {
      case e: Exception => c.rollback()
        throw e
    }
    c.setAutoCommit(true)
  }

  override def insertOrUpdate(call: CallData)(implicit c: Connection): CallData = {
    if (call.id.isDefined) update(call)
    else insert(call)
  }

  def insert(call: CallData)(implicit c: Connection): CallData = {
    c.setAutoCommit(false)
    try {
      val id: Option[Long] = callDataInsertStmt.on(
        'uniqueid -> call.uniqueId, 'dst_num -> call.dstNum, 'call_direction -> call.callDirection.toString, 'start_time -> call.startTime.toDate,
        'answer_time -> call.answerTime.map(_.toDate), 'end_time -> call.endTime.map(_.toDate), 'status -> call.status, 'ring_duration_on_answer -> call.ringDurationOnAnswer,
        'transfered -> call.transfered, 'src_num -> call.srcNum, 'transfer_direction -> call.transferDirection.map(_.toString),
        'src_agent -> call.srcAgent, 'dst_agent -> call.dstAgent, 'src_interface -> call.srcInterface).executeInsert()
      for (data <- call.attachedData) {
        attachedDataInsertStmt.on('id_call_data -> id, 'key -> data.key, 'value -> data.value).executeInsert()
      }
      for (period <- call.holdPeriods) {
        holdPeriodsInsertStmt.on('linkedid -> period.linkedId, 'start -> period.start.toDate, 'end -> period.end.map(_.toDate)).executeInsert()
      }
      c.commit()
      c.setAutoCommit(true)
      call.copy(id=id.map(_.toInt))
    }  catch {
      case e: Exception => c.rollback()
        logger.error("Got an exception while creating the call data ", e)
        c.setAutoCommit(true)
        call
    }
  }

  def update(call: CallData)(implicit c: Connection): CallData = {
    c.setAutoCommit(false)
    try {
      SQL("DELETE FROM attached_data WHERE id_call_data = {id}").on('id -> call.id).executeUpdate()
      SQL("DELETE FROM hold_periods WHERE linkedid = (SELECT uniqueid FROM call_data WHERE id = {id})").on('id -> call.id).executeUpdate()
      callDataUpdateStmt.on('uniqueid -> call.uniqueId, 'dst_num -> call.dstNum, 'call_direction -> call.callDirection.toString, 'start_time -> call.startTime.toDate,
        'answer_time -> call.answerTime.map(_.toDate), 'end_time -> call.endTime.map(_.toDate), 'status -> call.status, 'ring_duration_on_answer -> call.ringDurationOnAnswer,
        'transfered -> call.transfered, 'src_num -> call.srcNum, 'transfer_direction -> call.transferDirection.map(_.toString),
        'src_agent -> call.srcAgent, 'dst_agent -> call.dstAgent, 'src_interface -> call.srcInterface, 'id -> call.id).executeInsert()
      for (data <- call.attachedData) {
        attachedDataInsertStmt.on('id_call_data -> call.id, 'key -> data.key, 'value -> data.value).executeInsert()
      }
      for (period <- call.holdPeriods) {
        holdPeriodsInsertStmt.on('linkedid -> period.linkedId, 'start -> period.start.toDate, 'end -> period.end.map(_.toDate)).executeInsert()
      }
      c.commit()
      c.setAutoCommit(true)
    }  catch {
      case e: Exception => c.rollback()
        logger.error("Got an exception while creating the call data ", e)
        c.setAutoCommit(true)
    }
    call
  }
}

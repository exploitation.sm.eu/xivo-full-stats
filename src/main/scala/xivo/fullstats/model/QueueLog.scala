package xivo.fullstats.model

import java.sql.Connection
import java.util.Date

import anorm.SqlParser._
import anorm._
import org.joda.time.DateTime

case class QueueLog(
  override val id: Int,
  override val time: DateTime,
  override val callid: String,
  queueName: String,
  agent: String,
  event: String,
  data1: Option[String],
  data2: Option[String],
  data3: Option[String],
  data4: Option[String],
  data5: Option[String]) extends CallEvent(id, callid, time)

object QueueLog extends LastIdTable {

  override val lastIdTable: String = "last_queue_log_id"

  def getMaxDate()(implicit conn: Connection): Option[DateTime] =
    SQL(s"SELECT to_timestamp(max(time), 'YYYY-MM-DD HH24:MI:SS.US') AS res FROM queue_log").as(get[Option[Date]]("res") *).head.map(new DateTime(_))
}

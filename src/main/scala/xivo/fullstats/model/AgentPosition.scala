package xivo.fullstats.model

import java.sql.Connection
import java.util.Date

import anorm.SqlParser._
import anorm._
import org.joda.time.DateTime

case class AgentPosition(agent: String, lineNumber: String, sda: Option[String], startTime: DateTime, endTime: Option[DateTime])

object AgentPosition {
  def sdaFromLineNum(line: String)(implicit c: Connection): Option[String] =
    SQL("""SELECT e1.exten AS exten FROM extensions e1 JOIN dialaction d ON e1.typeval = d.categoryval JOIN extensions e2 ON e2.typeval = d.actionarg1
        WHERE e1.type = 'incall' AND d.category = 'incall' AND d.action = 'user' AND e2.type = 'user' AND e2.exten = {line} LIMIT 1""")
    .on('line -> line).as(get[String]("exten") *).headOption

  val simple = get[String]("line_number") ~
    get[Option[String]]("sda") ~
    get[String]("agent_num") ~
    get[Date]("start_time") ~
    get[Option[Date]]("end_time")  map {
    case line ~ sda ~ agent ~ start ~ end => AgentPosition(agent, line, sda, new DateTime(start), end.map(d => new DateTime(d)))
  }

  def lastPendingForAgent(agent: String)(implicit c: Connection): Option[AgentPosition] =
    SQL("SELECT line_number, sda, agent_num, start_time, end_time FROM agent_position WHERE agent_num = {agent} AND end_time IS NULL ORDER BY start_time DESC LIMIT 1")
      .on('agent -> agent).as(simple *).headOption

  def getMaxDate()(implicit c: Connection): Option[DateTime] = SQL("SELECT greatest(max(start_time), max(end_time)) AS res FROM agent_position")
    .as(get[Option[Date]]("res") *).head.map(new DateTime(_))

  def insertOrUpdate(p: AgentPosition)(implicit c: Connection): Unit = {
    if(positionExists(p)) update(p)
    else insert(p)
  }

  def positionExists(p: AgentPosition)(implicit c: Connection): Boolean = SQL("SELECT agent_num FROM agent_position WHERE agent_num = {agent} and start_time = {start}")
    .on('agent -> p.agent, 'start -> p.startTime.toDate).as(get[String]("agent_num") *).nonEmpty

  private def insert(p: AgentPosition)(implicit c: Connection): Unit =
    SQL("INSERT INTO agent_position(line_number, sda, agent_num, start_time, end_time) VALUES ({line}, {sda}, {agent}, {start}, {end})")
      .on('line -> p.lineNumber, 'sda -> p.sda, 'agent -> p.agent, 'start -> p.startTime.toDate, 'end -> p.endTime.map(_.toDate))
      .executeUpdate()

  private def update(p: AgentPosition)(implicit c: Connection): Unit =
    SQL("UPDATE agent_position SET line_number={line}, sda={sda}, end_time={end} WHERE agent_num={agent} AND start_time={start}")
      .on('line -> p.lineNumber, 'sda -> p.sda, 'end -> p.endTime.map(_.toDate), 'agent -> p.agent, 'start -> p.startTime.toDate)
      .executeUpdate()

  def agentForNumAndTime(number: String, time: DateTime)(implicit c: Connection): Option[String] = SQL("SELECT agent_num FROM agent_position " +
      "WHERE start_time <= {time} AND (end_time >= {time} OR end_time IS NULL) AND (line_number = {number}  OR sda = {number}) LIMIT 1")
      .on('number -> number, 'time -> time.toDate).as(get[String]("agent_num") *).headOption
}

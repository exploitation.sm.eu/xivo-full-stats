package xivo.fullstats.model

object SqlUtils {

  def createInClauseOrTrue(column: String, values: List[_]): String = createInClauseOrOption(column, values, "TRUE")

  private def createInClauseOrOption(column: String, values: List[_], replacement: String): String = {
    var clause = ""
    if(values.size == 0)
      clause = replacement
    else {
      clause = s"$column IN ("
      values.foreach({
        case i: Int => clause += s"$i,"
        case s: String => clause += s"'$s',"
      })
      clause = clause.substring(0, clause.length - 1)
      clause += ")"
    }
    clause
  }

  def createInClauseOrFalse(column: String, values: List[_]) = createInClauseOrOption(column, values, "FALSE")

}

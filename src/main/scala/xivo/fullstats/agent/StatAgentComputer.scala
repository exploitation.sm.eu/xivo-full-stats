package xivo.fullstats.agent

import java.sql.Connection

import org.joda.time.{DateTime, Period}
import org.slf4j.LoggerFactory
import xivo.fullstats.TimeUtils
import xivo.fullstats.model.{State, QueueLog, StatAgentPeriodic, AgentState}

import scala.collection.mutable.ListBuffer

class StatAgentComputer(val agent: String, var agentState: AgentState, var lastPeriod: StatAgentPeriodic, interval: Period)(implicit c: Connection) {

  val logger = LoggerFactory.getLogger(getClass)

  def notifyNewInterval(date: DateTime): List[StatAgentPeriodic] = {
    val newPeriods = generatePeriods(agentState.date, date, agent)
    val allPeriods = lastPeriod :: newPeriods
    allPeriods.foreach(p => propagateState(agentState, p, date))
    lastPeriod = allPeriods.last
    agentState = agentState.copy(date=date)
    allPeriods.take(allPeriods.size - 1).filterNot(_.isEmpty)
  }

  def processQueueLog(ql: QueueLog): Unit = {
    propagateState(agentState, lastPeriod, ql.time)
    ql.event match {
      case "AGENTCALLBACKLOGIN" => agentState = AgentState(ql.time, State.LoggedOn)
      case "AGENTCALLBACKLOGOFF" => agentState = AgentState(ql.time, State.LoggedOff)
      case "PAUSEALL" => agentState = AgentState(ql.time, State.Paused)
      case "UNPAUSEALL" => agentState = AgentState(ql.time, State.LoggedOn)
      case "WRAPUPSTART" =>   agentState = AgentState(ql.time, agentState.state)
        lastPeriod.wrapupTime += Integer.parseInt(ql.data1.get)
    }

  }

  def saveState(maxDate: DateTime): Unit = {
    val allPeriods = lastPeriod::generatePeriods(agentState.date, maxDate, agent)
    allPeriods.foreach(p => propagateState(agentState, p, maxDate))
    val nonEmptyPeriods = allPeriods.filterNot(_.isEmpty)
    logger.info(s"Saving buffered periods : $nonEmptyPeriods")
    StatAgentPeriodic.insertAll(nonEmptyPeriods)
    val updatedState = agentState.copy(date=maxDate)
    logger.info(s"Saving the agent state $updatedState")
    AgentState.replaceState(agent, updatedState)
  }

  def generatePeriods(start: DateTime, end: DateTime, agentNum: String): List[StatAgentPeriodic] = {
    var realStart = TimeUtils.roundToInterval(start, interval).plus(interval)
    val realEnd = TimeUtils.roundToInterval(end, interval)
    val res = ListBuffer[StatAgentPeriodic]()
    while(realStart.isBefore(realEnd) || realStart.equals(realEnd)) {
      res.append(StatAgentPeriodic(None, realStart, agentNum, 0, 0, 0))
      realStart = realStart.plus(interval)
    }
    res.toList
  }

  def propagateState(state: AgentState, period: StatAgentPeriodic, maxDate: DateTime): Unit = {
    val intervalStart = TimeUtils.max(period.time, state.date)
    val intervalEnd = TimeUtils.min(period.time.plus(interval), maxDate)
    val elapsedSeconds = new Period(intervalStart, intervalEnd).toStandardSeconds.getSeconds
    state.state match {
      case State.Paused =>
        period.loginTime += elapsedSeconds
        period.pauseTime += elapsedSeconds
      case State.LoggedOn =>
        period.loginTime += elapsedSeconds
      case _ =>
    }
  }

}

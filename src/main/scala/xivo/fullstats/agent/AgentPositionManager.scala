package xivo.fullstats.agent

import java.sql.Connection

import org.joda.time.DateTime
import org.slf4j.LoggerFactory
import xivo.fullstats.EventParser
import xivo.fullstats.model.{AgentPosition, QueueLog}

import scala.collection.mutable.{Map => MutableMap}

class AgentPositionManager()(implicit conn: Connection) extends EventParser[QueueLog] {
  val positions = new AgentPositionBuffer()
  val logger = LoggerFactory.getLogger(getClass)

  def parseEvent(e: QueueLog): Unit = {
    if(e.event.equals("AGENTCALLBACKLOGIN")) {
      logger.info(s"Processing agent login : $e")
      val lineNum = extractLineNumber(e.data1.get)
      positions.append(AgentPosition(extractAgent(e.agent), lineNum, AgentPosition.sdaFromLineNum(lineNum), new DateTime(e.time), None))
    } else if(e.event.equals("AGENTCALLBACKLOGOFF")) {
      logger.info(s"Processing agent logout : $e")
      val agent = extractAgent(e.agent)
      positions.getLastPosition(agent) match {
        case Some(position) if position.endTime.isEmpty =>  positions.removeLastPosition(agent)
          val updatedPosition = position.copy(endTime = Some(new DateTime(e.time)))
          logger.info(s"Inserting agent position $updatedPosition")
          AgentPosition.insertOrUpdate(updatedPosition)
        case _ =>
      }
    }
  }

  def extractAgent(agentStr: String) = agentStr.replace("Agent/", "")

  def extractLineNumber(qualifiedExten: String): String = if(qualifiedExten.contains("@")) qualifiedExten.substring(0, qualifiedExten.indexOf("@")) else qualifiedExten

  override def saveState(): Unit = {
    logger.info(s"Saving ongoing agent positions : ${positions.allPositions()}")
    positions.allPositions().foreach(AgentPosition.insertOrUpdate)
  }
}

class AgentPositionBuffer()(implicit c: Connection) {
  AgentPositionProxy.register(this)

  val map = MutableMap[String, AgentPosition]()

  def append(position: AgentPosition) = map.put(position.agent, position)

  def removeLastPosition(agent: String) = map.remove(agent)

  def allPositions(): List[AgentPosition] = map.values.toList

  def getLastPosition(agent: String): Option[AgentPosition] = map.get(agent) match {
    case None => val res = AgentPosition.lastPendingForAgent(agent)
      if(res.isDefined)
        map.put(agent, res.get)
      res
    case Some(position) => Some(position)
  }

  def allAgents(): List[String] = map.keys.toList
}

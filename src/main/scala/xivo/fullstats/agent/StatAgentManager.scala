package xivo.fullstats.agent

import java.sql.Connection

import org.joda.time.{DateTime, Period}
import org.slf4j.LoggerFactory
import xivo.fullstats.model.{AgentState, QueueLog, StatAgentPeriodic, State}
import xivo.fullstats.{EventParser, TimeUtils}

import scala.collection.mutable

class StatAgentManager(interval: Period)(implicit conn: Connection) extends EventParser[QueueLog] {
  val agentPattern = "Agent/(\\d+)".r
  val AgentEvents = List("AGENTCALLBACKLOGIN", "AGENTCALLBACKLOGOFF", "WRAPUPSTART", "PAUSEALL", "UNPAUSEALL")
  val logger = LoggerFactory.getLogger(getClass)

  val agents = mutable.Map[String, StatAgentComputer]()
  var currentInterval: Option[DateTime] = None

  override def saveState(): Unit = {
    QueueLog.getMaxDate() match {
      case Some(lastDate) => agents.values.foreach(_.saveState(lastDate))
      case None =>
    }
  }

  def extractAgentNumber(s: String): String = {
    agentPattern.findAllIn(s).matchData foreach {
      m => return m.group(1)
    }
    s
  }

  override def parseEvent(e: QueueLog): Unit = {
    val roundedTime = TimeUtils.roundToInterval(e.time, interval)
    if(currentInterval.isEmpty)
      currentInterval = Some(roundedTime)
    else if(roundedTime.isAfter(currentInterval.get)) {
      agents.values.foreach(c => {
        val oldPeriods = c.notifyNewInterval(e.time)
        logger.info(s"Saving periods of type stat_agent_periodic : $oldPeriods on date ${e.time}")
        StatAgentPeriodic.insertAll(oldPeriods)
      })
      currentInterval = Some(roundedTime)
    }
    if(AgentEvents.contains(e.event))
      realParseEvent(e)
  }

  def realParseEvent(ql: QueueLog): Unit = {
    val agentNum = extractAgentNumber(ql.agent)
    if(!agents.contains(agentNum)) {
      processNewAgent(agentNum, ql)
      StatAgentPeriodic.insertAll(agents(agentNum).notifyNewInterval(ql.time))
    }
    agents(agentNum).processQueueLog(ql)
  }

  def processNewAgent(agentNum: String, ql: QueueLog): Unit = {
    logger.info(s"A new agent $agentNum was found in the queue_log $ql")
    val lastPeriod = StatAgentPeriodic.lastForAgent(agentNum) match {
      case None => StatAgentPeriodic(None, TimeUtils.roundToInterval(ql.time, interval), agentNum, 0, 0, 0)
      case Some(period) => period
    }
    val state = AgentState.lastForAgent(agentNum) match {
      case None => ql.event match {
        case "AGENTCALLBACKLOGIN" => AgentState(TimeUtils.roundToInterval(ql.time, interval), State.LoggedOff)
        case _ => AgentState(TimeUtils.roundToInterval(ql.time, interval), State.LoggedOn)
      }
      case Some(s) => s
    }
    // should not happen
    if(state.date.isAfter(lastPeriod.time.plus(interval))) {
      val period = StatAgentPeriodic(None, TimeUtils.roundToInterval(state.date, interval), agentNum, 0, 0, 0)
      agents.put(agentNum, new StatAgentComputer(agentNum, state, period, interval))
    } else {
      agents.put(agentNum,  new StatAgentComputer(agentNum, state, lastPeriod, interval))
    }
  }
}

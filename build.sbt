import AssemblyKeys._
import com.typesafe.sbt.packager.docker._
import NativePackagerHelper._
import org.clapper.sbt.editsource.EditSourcePlugin.autoImport._
import org.clapper.sbt.editsource.EditSourcePlugin

name := "xivo-full-stats"

version := "2.1.4"

lazy val root = (project in file(".")).enablePlugins(DockerPlugin).enablePlugins(JavaServerAppPackaging)

scalaVersion := "2.11.6"

resolvers += "Typesafe Repository" at "http://repo.typesafe.com/typesafe/releases/"

libraryDependencies ++= Seq("org.scalatest" %% "scalatest" % "2.2.4" % "test",
                            "org.postgresql" % "postgresql" % "9.4-1201-jdbc41",
                            "com.typesafe.play" %% "anorm" % "2.3.6",
                            "joda-time" % "joda-time" % "2.0",
                            "org.joda" % "joda-convert" % "1.2",
                            "org.dbunit" % "dbunit" % "2.4.7" % "test",
                            "org.easymock" % "easymock" % "3.1" % "test",
                            "ch.qos.logback" % "logback-classic" % "1.0.7",
                            "org.rogach" %% "scallop" % "0.9.5",
                            "commons-configuration" % "commons-configuration" % "1.7",
                            "com.typesafe" % "config" % "1.2.1")

parallelExecution in Test := false

publishArtifact in (Compile, packageDoc) := false

mainClass in Compile := Some("xivo.fullstats.Main")

assemblySettings

mergeStrategy in assembly <<= (mergeStrategy in assembly) {
  (old) => {
    case PathList("org", "apache", "commons", xs@_*) => MergeStrategy.first
    case x => old(x)
  }
}

test in assembly := {}

dockerCommands += Cmd("ENV", "DB_NAME xivo_stats")

dockerCommands += Cmd("ENV", "DB_USERNAME asterisk")

dockerCommands += Cmd("ENV", "DB_PASSWORD proformatique")

dockerCommands += Cmd("ADD", "./opt/docker/sqlscripts/ $LIQUIBASE_HOME/sqlscripts")

maintainer in Docker := "Jean Aunis <jaunis@avencall.com>"

dockerBaseImage := "xivoxc/javaliquibase:1.1"

dockerRepository := Some("xivoxc")

dockerExposedVolumes := Seq("/var/log")

dockerEntrypoint := Seq("/usr/local/bin/start.sh","/opt/docker/bin/xivo-full-stats-docker")

val setVersionVarTask = Def.task {
    System.setProperty("XIVO_STATS_VERSION", version.value)
}

edit in EditSource <<= (edit in EditSource) dependsOn (EditSourcePlugin.autoImport.clean in EditSource)

packageBin in Compile <<= (packageBin in Compile) dependsOn (edit in EditSource)

run in Compile <<= (run in Compile) dependsOn setVersionVarTask

flatten in EditSource := true

targetDirectory in EditSource <<= baseDirectory / "conf"

variables in EditSource <+= version {version => ("SBT_EDIT_APP_VERSION", version)}

(sources in EditSource) <++= baseDirectory map { bd =>
    (bd / "src/res" * "appli.version").get
}

mappings in Universal += file("conf/appli.version") -> "conf/appli.version"
